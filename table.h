#ifndef TABLE_H
#define TABLE_H

#include "util.h"
#include "IR.h"
#define PRIME 97

typedef struct Hash_Node_def
{
	char *id;
	void *data;
	struct Hash_Node_def *next;
	IR_RECORD rec;
} Hash_Node;

typedef Hash_Node *Hash_Map[PRIME];

int hash(char *);
void *h_look(char *, Hash_Map);
void h_enter(char *, void *, IR_RECORD, Hash_Map);

extern IR_RECORD recbuf;

#endif
