O   [0-7]
D   [0-9]
L   [a-zA-Z_$]
H   [a-fA-F0-9]
WS  [ \t\v\n\f\r]
X   .


%{
#include <stdio.h>
#include "util.h"
#include "node.h"
#include "c.tab.h"
void comment(void);
void print(const char*);
void printerr(const char*);
void count();
int row = 0;
int column = 0;
extern FILE* tokens;
%}

%%
"/*"			{ comment(); }
"//"{X}*[\n]    { count();}
^{WS}*"#"[^\n]*	{ count();}

"break"			{ print("Key"); count(); return(BREAK_t); }
"continue"		{ print("Key"); count(); return(CONTINUE_t); }
"else"			{ print("Key"); count(); return(ELSE_t); }
"for"		   	{ print("Key"); count(); return(FOR_t); }
"char"			{ print("Key"); count(); return(CHAR_t); }
"if"		  	{ print("Key"); count(); return(IF_t); }
"int"			{ print("Key"); count(); return(INT_t); }
"return"		{ print("Key"); count(); return(RETURN_t); }
"sizeof"		{ print("Key"); count(); return(SIZEOF_t); }
"struct"		{ print("Key"); count(); return(STRUCT_t); }
"union"			{ print("Key"); count(); return(UNION_t); }
"void"			{ print("Key"); count(); return(VOID_t); }
"while"			{ print("Key"); count(); return(WHILE_t); }

0[xX]{H}+				{ print("Hex"); yylval.iValue = (int)strtol(yytext, NULL, 16); count(); return(INT_CONSTANT); }
0{O}*					{ print("Oct"); yylval.iValue = (int)strtol(yytext, NULL, 8); count(); return(INT_CONSTANT); }
[1-9]{D}*				{ print("Dec"); yylval.iValue = atoi(yytext); count(); return(INT_CONSTANT); }

\'[^\'\\\n]\'		{ print("Char"); yylval.iValue = yytext[1]; count(); return(CHAR_CONSTANT); }

\'\\a\'			{ print("Char"); yylval.iValue = '\a'; count(); return(CHAR_CONSTANT); }
\'\\b\'			{ print("Char"); yylval.iValue = '\b'; count(); return(CHAR_CONSTANT); }
\'\\f\'			{ print("Char"); yylval.iValue = '\f'; count(); return(CHAR_CONSTANT); }
\'\\n\'			{ print("Char"); yylval.iValue = '\n'; count(); return(CHAR_CONSTANT); }
\'\\t\'			{ print("Char"); yylval.iValue = '\t'; count(); return(CHAR_CONSTANT); }
\'\\v\'			{ print("Char"); yylval.iValue = '\v'; count(); return(CHAR_CONSTANT); }
\'\\r\'			{ print("Char"); yylval.iValue = '\r'; count(); return(CHAR_CONSTANT); }
\'\\{O}{1,3}\'	{ print("Char"); char tmp[4]; strncpy(tmp, yytext + 2, strlen(yytext) - 3); tmp[strlen(yytext)-3] = '\0'; yylval.iValue = (int)strtol(tmp, NULL, 8); count(); 
return(CHAR_CONSTANT); }
\'\\x{H}{1,2}\'	{ print("Char"); char tmp[4]; strncpy(tmp, yytext + 3, strlen(yytext) - 4); tmp[strlen(yytext)-4] = '\0'; yylval.iValue = (int)strtol(tmp, NULL, 16); count(); return(CHAR_CONSTANT); }


\"(\\.|[^\\"\n])*\"	{ print("Str"); yylval.str = strdup(yytext); count(); return(STRING_LITERAL); }

">>="			{ print("Op"); count(); return(RIGHT_ASSIGN); }
"<<="			{ print("Op"); count(); return(LEFT_ASSIGN); }
"+="			{ print("Op"); count(); return(ADD_ASSIGN); }
"-="			{ print("Op"); count(); return(SUB_ASSIGN); }
"*="			{ print("Op"); count(); return(MUL_ASSIGN); }
"/="			{ print("Op"); count(); return(DIV_ASSIGN); }
"%="			{ print("Op"); count(); return(MOD_ASSIGN); }
"&="			{ print("Op"); count(); return(AND_ASSIGN); }
"^="			{ print("Op"); count(); return(XOR_ASSIGN); }
"|="			{ print("Op"); count(); return(OR_ASSIGN); }
">>"			{ print("Op"); count(); return(RIGHT_OP); }
"<<"			{ print("Op"); count(); return(LEFT_OP); }
"++"			{ print("Op"); count(); return(INC_OP); }
"--"			{ print("Op"); count(); return(DEC_OP); }
"->"			{ print("Op"); count(); return(PTR_OP); }
"&&"			{ print("Op"); count(); return(AND_OP); }
"||"			{ print("Op"); count(); return(OR_OP); }
"<="			{ print("Op"); count(); return(LE_OP); }
">="			{ print("Op"); count(); return(GE_OP); }
"=="			{ print("Op"); count(); return(EQ_OP); }
"!="			{ print("Op"); count(); return(NE_OP); }
";"				{ print("Op"); count(); return(';'); }
("{"|"<%")		{ print("Op"); count(); return('{'); }
("}"|"%>")		{ print("Op"); count(); return('}'); }
","				{ print("Op"); count(); return(','); }
":"				{ print("Op"); count(); return(':'); }
"="				{ print("Op"); count(); return('='); }
"("				{ print("Op"); count(); return('('); }
")"				{ print("Op"); count(); return(')'); }
("["|"<:")		{ print("Op"); count(); return('['); }
("]"|":>")		{ print("Op"); count(); return(']'); }
"."				{ print("Op"); count(); return('.'); }
"&"				{ print("Op"); count(); return('&'); }
"!"				{ print("Op"); count(); return('!'); }
"~"				{ print("Op"); count(); return('~'); }
"-"				{ print("Op"); count(); return('-'); }
"+"				{ print("Op"); count(); return('+'); }
"*"				{ print("Op"); count(); return('*'); }
"/"				{ print("Op"); count(); return('/'); }
"%"				{ print("Op"); count(); return('%'); }
"<"				{ print("Op"); count(); return('<'); }
">"				{ print("Op"); count(); return('>'); }
"^"				{ print("Op"); count(); return('^'); }
"|"				{ print("Op"); count(); return('|'); }

{L}({L}|{D})*	{ yylval.str = strdup(yytext); print("Id"); count(); return IDENTIFIER;}

{WS}	    	{count();}

{X}				{ printerr("illegal character");}

%%

int yywrap(void)
{
	return 1;
}

void comment(void)
{
	char c, prev = 0;
	column = 0;
	while ((c = input()) > 0)      /* (EOF maps to 0) */
	{
		if (c == '\n')
			++row;
		if (c == '/' && prev == '*')
			return;
		prev = c;
	}
	printerr("unterminated comment");
}

void print(const char* str)
{
    fprintf(tokens, "<%s %s>\n", str, yytext);
}

void count()
{
	int i;
	for (i = 0; yytext[i] != '\0'; i++)
		if (yytext[i] == '\n')
			column = 0, ++row;
		else if (yytext[i] == '\t')
			column += 8 - (column % 8);
		else
			column++;
}
