#ifndef ENV_H
#define ENV_H

#include "util.h"
#include "table.h"
#include "types.h"
#include "IR.h"

#define MAX_DEPTH 500

struct ENV_DEF
{
    Hash_Map tentry[MAX_DEPTH];
    Hash_Map ventry[MAX_DEPTH];
};

void begin_scope();
void end_scope();
void hold_begin_scope();
void hold_end_scope();
void clean_scope(int);
IR_RECORD add_var(char *, Type *);
IR_RECORD add_var_sym(char *, Type *);
void add_type(char *, Type *);
void def_type(char *, Type *);
Type *look_var(char *);
Type *look_type(char *);
Type *look_var_top(char *);
Type *look_type_top(char *);

extern IR *mainIR;
extern IR *globalIR;

#endif
