#include "prtnode.h"

void print_type(node_type n)
{
    switch (n)
    {
        case program:
            {fprintf(ast, "prog");break;}
        case declaration1:
        case declaration2:
            {fprintf(ast, "decl");break;}
        case function_definition1:case function_definition2:
            {fprintf(ast, "func_def");break;}
        case parameters:
            {fprintf(ast, "param");break;}
        case declarator_list:
            {fprintf(ast, "decl_ls");break;}
        case init_declarator_list:
            {fprintf(ast, "init_decl_ls");break;}
        case init_declarator:
            {fprintf(ast, "init_decl");break;}
        case initializer_list:
            {fprintf(ast, "init_ls");break;}
        case type_specifier1:
            {fprintf(ast, "void");break;}
        case type_specifier2:
            {fprintf(ast, "char");break;}
        case type_specifier3:
            {fprintf(ast, "int");break;}         
	case type_specifier4:case type_specifier5:case type_specifier6:
            {fprintf(ast, "type_spec");break;}
        case type_declaration_list:
            {fprintf(ast, "type_decl_ls");break;}
        case type_declaration:
            {fprintf(ast, "type_decl");break;}
        case struct_or_union1:
            {fprintf(ast, "struct");break;}
        case struct_or_union2:
            {fprintf(ast, "union");break;}
        case plain_declaration:
            {fprintf(ast, "plain_decl");break;}
        case declarator1:case declarator2:
            {fprintf(ast, "decl");break;}
        case declarator_array:
            {fprintf(ast, "decl_arr");break;}
        case plain_declarator:
            {fprintf(ast, "plain_decl");break;}
        case pointer:
            {fprintf(ast, "ptr");break;}
        case statement:
            {fprintf(ast, "stat");break;}
        case compound_statement:
            {fprintf(ast, "comp_stat");break;}
        case block_item_list:
            {fprintf(ast, "block_item_ls");break;}
        case expression_statement:
            {fprintf(ast, "expr_stat");break;}
        case selection_statement1:case selection_statement2:
            {fprintf(ast, "if");break;}
        case iteration_statement1:
	    {fprintf(ast, "while");break;}
	case iteration_statement2:case iteration_statement3:
            {fprintf(ast, "for");break;}        	    
        case jump_statement1:
            {fprintf(ast, "continue");break;}
        case jump_statement2:
            {fprintf(ast, "break");break;}
        case jump_statement3:case jump_statement4:
            {fprintf(ast, "return");break;}
        case primary_expression1:case primary_expression2:case primary_expression3:case primary_expression4:
            {fprintf(ast, "prim_expr");break;}
        case postfix_expression1:
            {fprintf(ast, "[]");break;}
        case postfix_expression2:
            {fprintf(ast, "()");break;}
        case postfix_expression3:
            {fprintf(ast, ".");break;}
        case postfix_expression4:
            {fprintf(ast, "->");break;}
        case postfix_expression5:
            {fprintf(ast, "post ++");break;}
        case postfix_expression6:
            {fprintf(ast, "post --");break;}
        case argument_expression_list:
            {fprintf(ast, "arg_expr_ls");break;}
        case unary_expression1:
            {fprintf(ast, "++");break;}
        case unary_expression2:
            {fprintf(ast, "--");break;}
        case unary_expression5:
            {fprintf(ast, "&");break;}
        case unary_expression6:
            {fprintf(ast, "*");break;}
        case unary_expression7:
            {fprintf(ast, "+");break;}
        case unary_expression8:
            {fprintf(ast, "-");break;}
        case unary_expression9:
            {fprintf(ast, "~");break;}
        case unary_expression10:
            {fprintf(ast, "!");break;}                        
        case unary_expression3:case unary_expression4:
            {fprintf(ast, "sizeof");break;} 
        case type_name:
            {fprintf(ast, "type_name");break;}
        case cast_expression:
            {fprintf(ast, "cast_expr");break;}
        case multiplicative_expression1:
            {fprintf(ast, "*");break;}
        case multiplicative_expression2:
            {fprintf(ast, "/");break;}
        case multiplicative_expression3:
            {fprintf(ast, "%%");break;}
        case additive_expression1:
            {fprintf(ast, "+");break;}
        case additive_expression2:
            {fprintf(ast, "-");break;}
        case shift_expression1:
            {fprintf(ast, "<<");break;}
        case shift_expression2:
            {fprintf(ast, ">>");break;}
        case relational_expression1:
            {fprintf(ast, "<");break;}
        case relational_expression2:
            {fprintf(ast, ">");break;}       
        case relational_expression3:
            {fprintf(ast, "<=");break;}
        case relational_expression4:
            {fprintf(ast, ">=");break;}                  
        case equality_expression1:
            {fprintf(ast, "==");break;}
        case equality_expression2:
            {fprintf(ast, "!=");break;}            
        case and_expression:
            {fprintf(ast, "and");break;}
        case exclusive_or_expression:
            {fprintf(ast, "xor");break;}
        case inclusive_or_expression:
            {fprintf(ast, "or");break;}
        case logical_and_expression:
            {fprintf(ast, "log_and");break;}
        case logical_or_expression:
            {fprintf(ast, "log_or");break;}
        case assignment_expression1:
            {fprintf(ast, "=");break;}
        case assignment_expression2:
            {fprintf(ast, "*=");break;}        
        case assignment_expression3:
            {fprintf(ast, "/=");break;}
        case assignment_expression4:
            {fprintf(ast, "%%=");break;}       
        case assignment_expression5:
            {fprintf(ast, "+=");break;}
        case assignment_expression6:
            {fprintf(ast, "-=");break;}        
        case assignment_expression7:
            {fprintf(ast, "<<=");break;}
        case assignment_expression8:
            {fprintf(ast, ">>=");break;}       
        case assignment_expression9:
            {fprintf(ast, "&=");break;}
        case assignment_expression10:
            {fprintf(ast, "^=");break;}        
        case assignment_expression11:
            {fprintf(ast, "|=");break;}
        case expression:
            {fprintf(ast, "expr");break;}
        default:
        	{break;}
    }
}


void print_node(AST_Node *p, int space)
{
    int i;
    for (i = 0; i < space; ++i)
        fprintf(ast,  "\t");
    if (p->ntype == literal_node)
    {
    	if (TN(p) == INT)
       		 fprintf(ast,  "(int %d)\n ", *((int*)(p->data)));
       	else if (TN(p) == CHAR)
        	fprintf(ast,  "(char %c)\n ", *((char*)(p->data)));
	    else if (TN(p) == ARRAY)
    	    fprintf(ast,  "(str %s)\n ", (char*)(p->data));
    }
    else if (p->ntype == identifier_node)
    	fprintf(ast,  "(id %s)\n ", (char*)(p->data));
    else if (nc(p))
    {
        fprintf(ast, "(");
        print_type(p->ntype);
        fprintf(ast, "\n");
        int i;
        for (i = 0 ; i < nc(p); ++i)
            print_node(p->child.list[i], space + 1);
        for (i = 0; i < space; ++i)
            fprintf(ast,  "\t");
        fprintf(ast,  ")\n");
    }
    else
    {
        fprintf(ast, "(");
        print_type(p->ntype);
        fprintf(ast, ")\n");
    }
}
