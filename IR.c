#include "IR.h"

IR_RECORD IR_new_const(int rec)
{
    IR_RECORD record;
    record.type = IR_CONST;
    record.record = rec;
    record.flag = 0;
    return record;
}

IR_RECORD IR_new_string(char* rec)
{
    IR_RECORD record;
    record.type = IR_CONST;
    record.recordstr = rec;
    record.flag = 1;
    return record;
}

IR_RECORD IR_new_address(int len)
{
    static int address = 0;
    IR_RECORD record;
    record.type = IR_ADDRESS;
    record.record = address ++;
    record.flag = len;
    return record;
}

IR_RECORD IR_new_label()
{
    static int label = 0;
    IR_RECORD record;
    record.type = IR_LABEL;
    record.record = label ++;
    return record;
}

IR_RECORD IR_new_symbol(int len)
{
    static int symbol = 0;
    IR_RECORD record;
    record.type = IR_SYMBOL;
    record.record = symbol ++;
    record.flag = len;
    return record;
}


IR * IR_new()
{
    IR *node;
    node = (IR *)malloc(sizeof(IR));
    node->size = 1;
    node->num = 0;
    node->instr = checked_malloc(sizeof(IR_INSTR));
    return node;
}

void IR_enlarge(IR *node)
{
    IR_INSTR *p;
    int i;
    if(node->num < node->size) return;
    p = checked_malloc(sizeof(IR_INSTR) * node->size * 2);
    for(i=0; i<node->size; i++)
    {
        p[i] = node->instr[i];
    }
    free(node->instr);
    node->instr = p;
    node->size *= 2;
}

IR * IR_makelist(IR_OP op, int num, ...)
{
    va_list ap;
    int i;
    IR *node;
    va_start(ap, num);
    node = IR_new();
    node->num = 1;
    for(i=0; i<num; i++)
    {
        node->instr[0].rec[i] = va_arg(ap, IR_RECORD);
    }
    node->instr[0].op = op;
    return node;
}

IR * IR_merge(int num, ...) //注意：merge后原节点的内容将被删除
{
    va_list ap;
    int i, j;
    IR *node;
    IR *p;
    va_start(ap, num);
    node = IR_new();
    node->num = 0;
    for(i=0; i<num; i++)
    {
        p = va_arg(ap, IR *);
        if (p)
        {
     	   for(j=0; j<p->num; j++)
    	    {
    	        node->num ++;
    	        IR_enlarge(node);
    	        node->instr[node->num-1] = p->instr[j];
     	   }
      	   free(p->instr);
      	   p->instr = NULL;
       	   p->num = p->size = 0;
        }
    }
    return node;
}

void prtIR(IR *ir)
{
    int i;
    int f;
    f = 0;
    IR_RECORD t, t1;
    for (i = 0; i < ir->num; ++i)
    {
    	IR_RECORD r1 = ir->instr[i].rec[0], r2 = ir->instr[i].rec[1], r3 = ir->instr[i].rec[2];
    	if (ir->instr[i].op != IR_ADS) f = 0;
    	switch (ir->instr[i].op)
    	{
    		case IR_LAB:printf("LABEL ");prtrec(r1);printf("\n");break;
			case IR_GOTO:printf("GOTO  ");prtrec(r1);printf("\n");break;
    		case IR_SHL:printf("SHL   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_SHR:printf("SHR   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_MUL:printf("MUL   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_DIV:printf("DIV   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_MOD:printf("MOD   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
   		 	case IR_ADD:printf("ADD   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
   			case IR_SUB:printf("SUB   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
   			case IR_AND:printf("AND   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_XOR:printf("XOR   ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_OR:if (neqr(r1, r2) || neqr(r1, r3) || neqr(r2, r3)) {printf("OR    ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");}break;
    		case IR_EQ:printf("EQ    ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_NE:printf("NE    ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_LE:printf("LE    ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_GE:printf("GE    ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_GT:printf("GT    ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
  		  	case IR_LT:printf("LT    ");prtrec(r1);prtrec(r2);prtrec(r3);printf("\n");break;
    		case IR_NOT:printf("NOT   ");prtrec(r1);prtrec(r2);printf("\n");break;
    		case IR_LNOT:printf("LNOT  ");prtrec(r1);prtrec(r2);printf("\n");break;
    		case IR_IF:printf("IF    ");prtrec(r1);prtrec(r2);printf("\n");break;
    		case IR_CALL:printf("CALL  ");prtrec(r1);printf("\n");break;
    		case IR_RET:printf("RET");printf("\n");break;
    		case IR_PUSH:printf("PUSH  ");prtrec(r1);printf("\n");break;
    		case IR_POP:printf("POP   ");prtrec(r1);printf("\n");break;
    		case IR_TINT:printf("TINT  ");prtrec(r1);prtrec(r2);printf("\n");break;
    		case IR_TCHAR:printf("TCHAR ");prtrec(r1);prtrec(r2);printf("\n");break;
    		case IR_MOVE:printf("MOVE  ");prtrec(r1);prtrec(r2);printf("\n");break;
    		case IR_ADS:if (!f || neqr(r2,t)){printf("ADS   ");prtrec(r1);prtrec(r2);}else{printf("MOVE  ");prtrec(r1);prtrec(t1);}printf("\n");f=0;break;
    		case IR_STAR:printf("STAR  ");f=1;prtrec(r1);prtrec(r2);t=r1;t1=r2;printf("\n");break;
    		case IR_FUNC:printf("FUNC  ");prtrec(r1);printf("\n");break;
    		case IR_END:printf("END");printf("\n");break;
    		case IR_AUG:printf("AUG   ");prtrec(r1);printf("\n");break;
    		case IR_GAUG:printf("GAUG  ");prtrec(r1);printf("\n");break;
    	}
	}
}

void prtrec(IR_RECORD rec)
{
	if (rec.type == IR_CONST)
	{
		if (rec.flag == 1)
		{
			printf(".%s ", rec.recordstr);
		}
		else
		{
			printf("d%d ", rec.record);
		}
		return;
	}
	switch (rec.type)
	{
		case IR_CONST:break;
		case IR_ADDRESS:printf("$(%d)", rec.flag);break;
		case IR_LABEL:printf("l");break;
		case IR_SYMBOL:printf("s(%d)", rec.flag);break;
	}
	printf("%d ", rec.record);
}

int neqr(IR_RECORD r1, IR_RECORD r2)
{
	if (r1.record != r2.record) return 1;
	if (r1.type != r2.type) return 1;
	if (r1.flag != r2.flag) return 1;
	return 0;
}

