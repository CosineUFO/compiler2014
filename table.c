#include "table.h"

IR_RECORD recbuf;

int hash(char *str)
{
	int ret = 0;
	char *c;
	for (c = str; *c != '\0'; ++c)
	{
		ret += (int)(*c);
		ret %= PRIME;
	}
	return ret;
}

void *h_look(char *str, Hash_Map map)
{
	int h = hash(str);
#ifdef DEBUG
	printf("%s %d %d\n", str, map, h);
#endif
	Hash_Node *p = map[h];
	while (p)
	{
		if (strcmp(p->id, str) == 0)
		{
			recbuf = p -> rec;
			return p -> data;
		}
		p = p -> next;
	}
#ifdef DEBUG
	printf("Fail\n", str, map, h);
#endif
	return NULL;
}

void h_enter(char *str, void *data, IR_RECORD rec, Hash_Map map)
{
	int h = hash(str);
	Hash_Node *p = checked_malloc(sizeof(Hash_Node));
#ifdef DEBUG
	printf("%s %d %d %d\n", str, data, map, h);
#endif
	p -> id = strdup(str);
	p -> data = data;                                                                                                                       
	p -> next = map[h];
	p -> rec = rec;
	map[h] = p;
}

