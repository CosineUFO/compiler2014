#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#define MAXALLOC 1000
#define M 200000
#define N 100
#define PAR_NUM 32
#define TTIFNUM 37

#define regN 17
int regr[regN];
char *regs[regN+1] = {"$s2", "$s3", "$s4", "$s5", "$s6", "$s7", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9", "$k0", "$k1", "$v1", "$fp"};

char tmpstr[100000];
int codenum = 0;
int globalvar[M];
int globalsymbol[M];
int globalstr[M];
int globallen = 0;
int staticvar[M];
int staticsymbol[M];
int staticlen = PAR_NUM*4;

int isstring[M];
int functionvar[M];
int functionsymbol[M];
int functionlen;
int freg[M];
int *in[M], *out[M], tin[M], tout[M], in1[M], out1[M], tin1, tout1;
int fleft[M], fright[M];
int vreg[M], vregn;

typedef struct NODE_IR_RECORD
{
    enum
    {
        IR_CONST,
        IR_ADDRESS,
        IR_LABEL,
        IR_SYMBOL,
        IR_UNDEFINED
    } type;
    int flag;
    union
    {
        int record;
        char *recordstr;
    };
} NODE_IR_RECORD;
typedef struct
{
    char *op;
    NODE_IR_RECORD rec[3];
} NODE_IR_INSTR;
typedef struct
{
    char *op;
    int num;
} NODE_IR_INSTR_FORMAT;

NODE_IR_INSTR_FORMAT ttinstrformat[] =
{
    {"LABEL", 1}, {"GOTO", 1}, {"SHL", 3}, {"SHR", 3}, {"MUL", 3},
    {"DIV", 3}, {"MOD", 3}, {"ADD", 3}, {"SUB", 3}, {"AND", 3},
    {"XOR", 3}, {"OR", 3}, {"LAND", 3}, {"LOR", 3}, {"EQ", 3},
    {"NE", 3}, {"LE", 3}, {"GE", 3}, {"GT", 3}, {"LT", 3},
    {"NOT", 2}, {"LNOT", 2}, {"IF", 2}, {"CALL", 1}, {"RET", 0},
    {"PUSH", 1}, {"POP", 1}, {"TINT", 2}, {"TCHAR", 2}, {"MOVE", 2},
    {"ADS", 2}, {"STAR", 2}, {"NEW", 2}, {"FUNC", 1}, {"END", 0},
    {"AUG", 1}, {"GAUG", 1},
};

NODE_IR_INSTR code[M];


void init(int argc, char *argv[])
{
    FILE *in;
    int i, j, k;
    memset(globalvar, -1, sizeof(globalvar));
    memset(globalsymbol, -1, sizeof(globalsymbol));
    memset(staticvar, -1, sizeof(staticvar));
    memset(staticsymbol, -1, sizeof(staticsymbol));
    in = stdin;
    while(fscanf(in, "%s", tmpstr) != EOF)
    {
        code[codenum].op = strdup(tmpstr);
        for(i=0; i<TTIFNUM; i++)
            if(strcmp(code[codenum].op, ttinstrformat[i].op) == 0) break;
        k = ttinstrformat[i].num;
        for(i=0; i<k; i++)
        {
            char t;
            fscanf(in, " %c", &t);
            switch(t)
            {
                case 'd':
                    code[codenum].rec[i].type = IR_CONST;
                    code[codenum].rec[i].flag = 0;
                    fscanf(in, "%d", &code[codenum].rec[i].record);
                    break;
                case '.':
                    code[codenum].rec[i].type = IR_CONST;
                    code[codenum].rec[i].flag = 1;
                    //fscanf(in, "%s", tmpstr);
                    j = 0;
                    fscanf(in, "%c", &tmpstr[j]);
                    do
                    {
                        j ++;
                        fscanf(in, "%c", &tmpstr[j]);
                    } while(!(tmpstr[j] == '\"' && tmpstr[j-1] != '\\'));
                    tmpstr[j+1] = 0;
                    code[codenum].rec[i].recordstr = strdup(tmpstr);
                    break;
                case '$':
                    code[codenum].rec[i].type = IR_ADDRESS;
                    fscanf(in, "(%d)%d", &code[codenum].rec[i].flag, &code[codenum].rec[i].record);
                    break;
                case 's':
                    code[codenum].rec[i].type = IR_SYMBOL;
                    fscanf(in, "(%d)%d", &code[codenum].rec[i].flag, &code[codenum].rec[i].record);
                    break;
                case 'l':
                    code[codenum].rec[i].type = IR_LABEL;
                    fscanf(in, "%d", &code[codenum].rec[i].record);
                    break;
            }
        }
        for(i=k; i<3; i++)
        {
            code[codenum].rec[i].type = IR_UNDEFINED;
        }
        codenum ++;
    }
    fclose(in);
}

typedef struct MIPS_CODE
{
    char *label;
    char *op;
    int num;
    struct MIPS_CODE_REC
    {
        enum {MIPS_REG, MIPS_LABEL, MIPS_NUMBER, MIPS_STRING} type;
        union
        {
            int integer;
            char *string;
        } record;
    } rec[3];
} MIPS_CODE;
MIPS_CODE mipscode[M];
int mipscodenum = 0;
int mipscodemnum = M-1;
int mipscodeaddflag = 0;
int maininsertpoint = 0;
MIPS_CODE mipscodehead[M/2];
int mipscodeheadnum = 0;

int newlabelnum = 0;

void mips_code_add(char *label, char *op, int num, ...)
{
    va_list ap;
    int i;
    va_start(ap, num);
    if(mipscodeaddflag == 0)
    {
        mipscode[mipscodenum].label = label;
        mipscode[mipscodenum].op = op;
        mipscode[mipscodenum].num = num;
        for(i=0; i<num; i++)
        {
            mipscode[mipscodenum].rec[i] = va_arg(ap, struct MIPS_CODE_REC);
        }
        mipscodenum ++;
    }
    else if(mipscodeaddflag == 1)
    {
        mipscode[mipscodemnum].label = label;
        mipscode[mipscodemnum].op = op;
        mipscode[mipscodemnum].num = num;
        for(i=0; i<num; i++)
        {
            mipscode[mipscodemnum].rec[i] = va_arg(ap, struct MIPS_CODE_REC);
        }
        mipscodemnum --;
    }
    else
    {
        mipscodehead[mipscodeheadnum].label = label;
        mipscodehead[mipscodeheadnum].op = op;
        mipscodehead[mipscodeheadnum].num = num;
        for(i=0; i<num; i++)
        {
            mipscodehead[mipscodeheadnum].rec[i] = va_arg(ap, struct MIPS_CODE_REC);
        }
        mipscodeheadnum ++;
    }
}

void interpret_gp_convert(int p)
{
    sprintf(tmpstr, "%d($gp)", p);
}

void interpret_sp_convert(int p)
{
    sprintf(tmpstr, "%d($sp)", p);
}

void interpret_load_ads(NODE_IR_RECORD rec, char *pos) //将rec存入寄存器pos
{
    struct MIPS_CODE_REC trec[3];
    if(rec.type == IR_SYMBOL)
    {
        if(globalsymbol[rec.record] == -1) //可修改符号
        {
            if(staticsymbol[rec.record] == -1) //局部符号
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                interpret_sp_convert(functionsymbol[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", "lw", 2, trec[0], trec[1]);
            }
            else
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                interpret_gp_convert(staticsymbol[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", "lw", 2, trec[0], trec[1]);
            }
        }
        else
        {
            trec[0].type = MIPS_REG;
            trec[0].record.string = pos;
            sprintf(tmpstr, "%d$(gp)", globalsymbol[rec.record]);
            trec[1].type = MIPS_REG;
            trec[1].record.string = strdup(tmpstr);
            mips_code_add("", "lw", 2, trec[0], trec[1]);
        }
    }
    else if(rec.type == IR_ADDRESS)
    {
        if(globalvar[rec.record] == -1) //可修改变量
        {
            if(staticvar[rec.record] == -1) //局部变量
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                interpret_sp_convert(functionvar[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", "la", 2, trec[0], trec[1]);
            }
            else
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                interpret_gp_convert(staticvar[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", "la", 2, trec[0], trec[1]);
            }
        }
        else
        {
            trec[0].type = MIPS_REG;
            trec[0].record.string = pos;
            sprintf(tmpstr, "%d($gp)", globalvar[rec.record]);
            trec[1].type = MIPS_REG;
            trec[1].record.string = strdup(tmpstr);
            mips_code_add("", "la", 2, trec[0], trec[1]);
        }
    }
    else
    {
        ;//不可能
    }
}

int getreg(NODE_IR_RECORD rec)
{
    if (rec.type == IR_ADDRESS)
        return freg[rec.record];
    return -1;
}

void interpret_load(NODE_IR_RECORD rec, char *pos) //将rec存入寄存器pos
{
    struct MIPS_CODE_REC trec[3];
    int rflag = getreg(rec);
    if (rflag >= 0)
    {
        trec[0].type = MIPS_REG;
        trec[0].record.string = pos;
        trec[1].type = MIPS_REG;
        trec[1].record.string = regs[rflag];
        mips_code_add("", "move", 2, trec[0], trec[1]);   
        return;         	
    }
    char *op;
    if(rec.flag == 1)
        op = "lb";
    else if(rec.flag == 4)
        op = "lw";
    else
        op = "lb";
    if(rec.type == IR_SYMBOL)
    {
        if(globalsymbol[rec.record] == -1) //可修改符号
        {
            if(staticsymbol[rec.record] == -1) //局部符号
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                interpret_sp_convert(functionsymbol[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", "lw", 2, trec[0], trec[1]);
                sprintf(tmpstr, "0(%s)", pos);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", op, 2, trec[0], trec[1]);
            }
            else //全局符号
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                interpret_gp_convert(staticsymbol[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", "lw", 2, trec[0], trec[1]);
                sprintf(tmpstr, "0(%s)", pos);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", op, 2, trec[0], trec[1]);
            }
        }
        else //不可修改符号
        {
            trec[0].type = MIPS_REG;
            trec[0].record.string = pos;
            sprintf(tmpstr, "%d($gp)", globalsymbol[rec.record]);
            trec[1].type = MIPS_REG;
            trec[1].record.string = strdup(tmpstr);
            mips_code_add("", "lw", 2, trec[0], trec[1]);
            sprintf(tmpstr, "0(%s)", pos);
            trec[1].type = MIPS_REG;
            trec[1].record.string = strdup(tmpstr);
            mips_code_add("", op, 2, trec[0], trec[1]);
        }
    }
    else if(rec.type == IR_ADDRESS)
    {
        if(globalvar[rec.record] == -1) //可修改变量
        {
            if(staticvar[rec.record] == -1) //局部变量
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                interpret_sp_convert(functionvar[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", op, 2, trec[0], trec[1]);
            }
            else //全局变量
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                interpret_gp_convert(staticvar[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", op, 2, trec[0], trec[1]);
            }
        }
        else //不可修改变量
        {
            trec[0].type = MIPS_REG;
            trec[0].record.string = pos;
            sprintf(tmpstr, "%d($gp)", globalvar[rec.record]);
            trec[1].type = MIPS_REG;
            trec[1].record.string = strdup(tmpstr);
            mips_code_add("", op, 2, trec[0], trec[1]);
        }
    }
    else if(rec.type == IR_CONST)
    {
        if(rec.flag == 1) //string
        {
            ; //不可能            
        }
        else
        {
            trec[0].type = MIPS_REG;
            trec[0].record.string = pos;
            trec[1].type = MIPS_NUMBER;
            trec[1].record.integer = rec.record;
            mips_code_add("", "li", 2, trec[0], trec[1]);
        }
    }
    else
    {
        ;//不可能
    }
}

char *interpret_load_reg(NODE_IR_RECORD rec, char *pos)
{
    if (rec.type == IR_ADDRESS && freg[rec.record]>=0)
        return regs[freg[rec.record]];
    interpret_load(rec, pos);
    return pos;
}


void interpret_store(NODE_IR_RECORD rec, char *pos) //将寄存器pos存入rec
{
    struct MIPS_CODE_REC trec[3];
    char *op;
    int i, k = 1;
    if(rec.flag == 1)
        op = "sb";
    else if(rec.flag == 4)
        op = "sw";
    else
    {
        op = "sb";
        k = rec.flag;
    }
    for(i=0; i<k && i<4; i++) //warning & to be continued 这里全局变量若空间大于4，则只会将前4位清零，但由于spim默认的$gp空间是0，所以这样做没有问题
    {
        if(rec.type == IR_SYMBOL)
        {
            if(globalsymbol[rec.record] == -1) //可修改符号
            {
                if(staticsymbol[rec.record] == -1) //局部符号
                {
                    trec[0].type = MIPS_REG;
                    trec[0].record.string = "$t0";
                    interpret_sp_convert(functionsymbol[rec.record]);
                    trec[1].type = MIPS_REG;
                    trec[1].record.string = strdup(tmpstr);
                    mips_code_add("", "lw", 2, trec[0], trec[1]);
                    trec[0].type = MIPS_REG;
                    trec[0].record.string = pos;
                    trec[1].type = MIPS_REG;
                    sprintf(tmpstr, "%d($t0)", i);
                    trec[1].record.string = strdup(tmpstr);
                    mips_code_add("", op, 2, trec[0], trec[1]);
                }
                else //全局符号
                {
                    trec[0].type = MIPS_REG;
                    trec[0].record.string = "$t0";
                    interpret_gp_convert(staticsymbol[rec.record]);
                    trec[1].type = MIPS_REG;
                    trec[1].record.string = strdup(tmpstr);
                    mips_code_add("", "lw", 2, trec[0], trec[1]);
                    trec[0].type = MIPS_REG;
                    trec[0].record.string = pos;
                    trec[1].type = MIPS_REG;
                    sprintf(tmpstr, "%d($t0)", i);
                    trec[1].record.string = strdup(tmpstr);
                    mips_code_add("", op, 2, trec[0], trec[1]);
                }
            }
            else //不可修改符号
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = "$t0";
                sprintf(tmpstr, "%d($gp)", globalsymbol[rec.record]);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", "lw", 2, trec[0], trec[1]);
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                trec[1].type = MIPS_REG;
                sprintf(tmpstr, "%d($t0)", i);
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", op, 2, trec[0], trec[1]);
            }
        }
        else if(rec.type == IR_ADDRESS)
        {
            if (freg[rec.record]>=0)
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                trec[1].type = MIPS_REG;
                sprintf(tmpstr, "%s", regs[freg[rec.record]]);
                trec[1].record.string = strdup(tmpstr); 
                mips_code_add("", "move", 2, trec[1], trec[0]);        	
            }
            else if(globalvar[rec.record] == -1) //可修改变量
            {

                if(staticvar[rec.record] == -1) //局部变量
                {
                    trec[0].type = MIPS_REG;
                    trec[0].record.string = pos;
                    interpret_sp_convert(functionvar[rec.record] + i);
                    trec[1].type = MIPS_REG;
                    trec[1].record.string = strdup(tmpstr); 
                    mips_code_add("", op, 2, trec[0], trec[1]);
                }
                else
                {
                    trec[0].type = MIPS_REG;
                    trec[0].record.string = pos;
                    interpret_gp_convert(staticvar[rec.record] + i);
                    trec[1].type = MIPS_REG;
                    trec[1].record.string = strdup(tmpstr); 
                    mips_code_add("", op, 2, trec[0], trec[1]);
                }
            }
            else //不可修改变量
            {
                trec[0].type = MIPS_REG;
                trec[0].record.string = pos;
                sprintf(tmpstr, "%d($gp)", globalvar[rec.record] + i);
                trec[1].type = MIPS_REG;
                trec[1].record.string = strdup(tmpstr);
                mips_code_add("", op, 2, trec[0], trec[1]);
            }
        }
        else
        {
            ;//不可能
        }
    }
}

void lregs(int xx)
{
    struct MIPS_CODE_REC rec[2];
    interpret_sp_convert(functionlen - 4);
    rec[0].type = MIPS_REG;
    rec[0].record.string = "$s0";
    rec[1].type = MIPS_REG;
    rec[1].record.string = strdup(tmpstr);
    mips_code_add("", "lw", 2, rec[0], rec[1]);

    interpret_sp_convert(functionlen - 8);
    rec[0].type = MIPS_REG;
    rec[0].record.string = "$s1";
    rec[1].type = MIPS_REG;
    rec[1].record.string = strdup(tmpstr);
    mips_code_add("", "lw", 2, rec[0], rec[1]);

    interpret_sp_convert(functionlen - 12);
    rec[0].type = MIPS_REG;
    rec[0].record.string = "$ra";
    rec[1].type = MIPS_REG;
    rec[1].record.string = strdup(tmpstr);
    mips_code_add("", "lw", 2, rec[0], rec[1]);

    int i;
    for (i=0; i<=xx; ++i)
    {
        interpret_sp_convert(functionlen - 16 - 4*i);
        rec[0].type = MIPS_REG;
        rec[0].record.string = regs[i];
        rec[1].type = MIPS_REG;
        rec[1].record.string = strdup(tmpstr);
        mips_code_add("", "lw", 2, rec[0], rec[1]);
    }
}

void sregs(int xx)
{
    struct MIPS_CODE_REC rec[2];
    interpret_sp_convert(functionlen - 4);
    rec[0].type = MIPS_REG;
    rec[0].record.string = "$s0";
    rec[1].type = MIPS_REG;
    rec[1].record.string = strdup(tmpstr);
    mips_code_add("", "sw", 2, rec[0], rec[1]);

    interpret_sp_convert(functionlen - 8);
    rec[0].type = MIPS_REG;
    rec[0].record.string = "$s1";
    rec[1].type = MIPS_REG;
    rec[1].record.string = strdup(tmpstr);
    mips_code_add("", "sw", 2, rec[0], rec[1]);

    interpret_sp_convert(functionlen - 12);
    rec[0].type = MIPS_REG;
    rec[0].record.string = "$ra";
    rec[1].type = MIPS_REG;
    rec[1].record.string = strdup(tmpstr);
    mips_code_add("", "sw", 2, rec[0], rec[1]);

    int i;
    for (i=0; i<=xx; ++i)
    {
        interpret_sp_convert(functionlen - 16 - 4*i);
        rec[0].type = MIPS_REG;
        rec[0].record.string = regs[i];
        rec[1].type = MIPS_REG;
        rec[1].record.string = strdup(tmpstr);
        mips_code_add("", "sw", 2, rec[0], rec[1]);
    }
}
int use(int x, int **a)
{
    *a = NULL;
    int k, kk;
    for(k=0; k<TTIFNUM; k++) if(strcmp(code[x].op, ttinstrformat[k].op) == 0) break;
    if (ttinstrformat[k].num==1 && code[x].rec[0].type == IR_ADDRESS && freg[code[x].rec[0].record] == -1)
    {
        *a=malloc(sizeof(int));
        (*a)[0]=code[x].rec[0].record;
        return 1;
    }
    int ta = 0;
    for(kk=ttinstrformat[k].num-1; kk>=1; kk--)
        if (code[x].rec[kk].type == IR_ADDRESS && freg[code[x].rec[kk].record] == -1)
            ++ta;
    *a=malloc(sizeof(int)*ta);
    int tb = 0;
    for(kk=ttinstrformat[k].num-1; kk>=1; kk--)
        if (code[x].rec[kk].type == IR_ADDRESS && freg[code[x].rec[kk].record] == -1)
            (*a)[tb++]=code[x].rec[kk].record;
    return ta;
}
int def(int x, int **a)
{
    *a = NULL;
    int k;
    for(k=0; k<TTIFNUM; k++) if(strcmp(code[x].op, ttinstrformat[k].op) == 0) break;
    if (ttinstrformat[k].num<2) return 0;
    if (code[x].rec[0].type == IR_ADDRESS && freg[code[x].rec[0].record] == -1)
    {
        *a=malloc(sizeof(int));
        (*a)[0]=code[x].rec[0].record;
        return 1;
    }
    return 0;
}
int findlb(int x, int s)
{
    int i;
    for (i=s; i<codenum; i++)
        if (strcmp(code[i].op, "LABEL") == 0 && code[i].rec[0].record == x)
            return i-s;
}
int setin(int *a, int ta, int n)
{
    int i;
    for (i=0;i<ta;++i)if (a[i]==n) return 1;
    return 0;
}
int setunion(int *a, int *b, int ta, int tb, int **c)
{
    int cc[M];
    int tc=ta;
    memcpy(cc,a,ta*sizeof(int));
    int i;
    for (i=0;i<tb;++i)
        if (!setin(cc,tc,b[i]))
            cc[tc++]=b[i];
    *c=malloc(tc*sizeof(int));
    memcpy(*c,cc,tc*sizeof(int));
    return tc;		
}
int setminus(int *a, int*b, int ta, int tb, int **c)
{
    int cc[M];
    int tc=0;
    int i;
    for (i=0;i<ta;++i)
        if (!setin(b,tb,a[i]))
            cc[tc++]=a[i];
    *c=malloc(tc*sizeof(int));
    memcpy(*c,cc,tc*sizeof(int));
    return tc;		
}

void interpret()
{
    int i, j, k;
    int pmain = -1;
    int level = 0;
    int flag = 0;
    int augcount = 0;
    int gaugcount = 0;
    int creg = 0;
    int cureg = -1;
    int rflag = -1;
    char *s0 = NULL;
    char *s1 = NULL;

    int globalflag=0;
    NODE_IR_RECORD globalrec;

    mipscodeaddflag = 2;
    for(i=0; i<codenum; i++)
    {
        int s;
        struct MIPS_CODE_REC rec[3];

        for(j=0; j<TTIFNUM; j++) if(strcmp(code[i].op, ttinstrformat[j].op) == 0) break;
        s = j;
        if(s == 29)
        {
            if(code[i].rec[0].type == IR_ADDRESS &&
                    code[i].rec[1].type == IR_CONST && 
                    code[i].rec[1].flag == 1)
            {
                rec[0].type = MIPS_STRING;
                rec[0].record.string = strdup(code[i].rec[1].recordstr);
                mips_code_add("", ".align 2", 0);
                mips_code_add("", ".asciiz", 1, rec[0]);
                isstring[code[i].rec[0].record]=1;

                globalvar[code[i].rec[0].record] = globallen;
                int slen = strlen(code[i].rec[1].recordstr);
                k=1;
                for (j=1;j<slen-1;++j)
                {
                    if (code[i].rec[1].recordstr[j] == '\\')
                        if (code[i].rec[1].recordstr[j+1] == 'n' || code[i].rec[1].recordstr[j+1] == 't')
                            ++j;
                    ++k;
                }
                globallen += k;
                while (globallen%4)++globallen;
                continue;
            }
        }
        if(level == 0 && s != 33 && ttinstrformat[s].num >= 1)
        {
            if(code[i].rec[0].type == IR_ADDRESS)
            {
                mips_code_add("", ".align 2", 0);
                rec[0].type = MIPS_NUMBER;
                rec[0].record.integer = code[i].rec[0].flag;
                mips_code_add("", ".space", 1, rec[0]);
                globalvar[code[i].rec[0].record] = globallen;
                globallen += code[i].rec[0].flag;
                while (globallen%4)++globallen;
                /*
                   staticvar[code[i].rec[0].record] = staticlen;
                   staticlen += code[i].rec[0].flag;
                   while(staticlen % 4) staticlen ++;
                   */
            }
            else if(code[i].rec[0].type == IR_SYMBOL)
            {

                mips_code_add("", ".align 2", 0);
                rec[0].type = MIPS_NUMBER;
                rec[0].record.integer = 4;
                mips_code_add("", ".space", 1, rec[0]);
                globalsymbol[code[i].rec[0].record] = globallen;
                globallen += 4;
                //staticsymbol[code[i].rec[0].record] = staticlen;
                //staticlen += 4;
            }
        }
        if(s == 33)
        {
            level ++;
        }
        if(s ==  34)
        {
            level --;
        }
    }
    for(i=0; i<PAR_NUM; i++)
    {
        staticvar[M-i-1] = - (i + 1) * 4;
    }    
    memset(freg, -1, sizeof(freg));
    for(i=0; i<codenum; i++)
    {
        int s;
        char *label;
        char *op;
        struct MIPS_CODE_REC rec[3];
        for(j=0; j<TTIFNUM; j++) if(strcmp(code[i].op, ttinstrformat[j].op) == 0) break;
        s = j;
        mipscodeaddflag = 0;
        if(s == 29 && code[i].rec[0].type == IR_ADDRESS &&
                code[i].rec[1].type == IR_CONST && 
                code[i].rec[1].flag == 1)
        {
            continue;
        }
        if(level == 0 && s != 33) //处理全局变量
        {
            if(s == 23) //CALL 后面对应的是main
            {
                pmain = i;
                continue;
            }
            mipscodeaddflag = 1;
        }
        if(mipscodeaddflag == 0 && flag == 0)
        {
            flag = 1;
            mips_code_add("", ".text", 0);
        }
        op = NULL;
        switch(s)
        {
            case 0: //LABEL
                sprintf(tmpstr, "l%d", code[i].rec[0].record);
                label = strdup(tmpstr);
                mips_code_add(label, "", 0);
                break;
            case 1: //GOTO
                rec[0].type = MIPS_LABEL;
                if(code[i].rec[0].record == code[pmain].rec[0].record)
                {
                    sprintf(tmpstr, "main");
                }
                else
                {
                    sprintf(tmpstr, "l%d", code[i].rec[0].record);
                }
                rec[0].record.string = strdup(tmpstr);
                mips_code_add("", "b", 1, rec[0]);
                break;
            case 2: //SHL
                if(!op) op = "sllv";
            case 3: //SHR
                if(!op) op = "srlv";
            case 4: //MUL
                if(!op) op = "mul";
            case 5: //DIV
                if(!op) op = "div"; //to be continued 注意这里需要做正负判断, 或者将这个判断放到translate中
            case 6: //MOD
                if(!op) op = "rem"; //to be continued 注意这里需要做正负判断, 或者将这个判断放到translate中
            case 7: //ADD
                if(!op)
                {
                    /*
                       if(code[i].rec[2].type == IR_CONST)
                       op = "addi";
                       else
                       */
                    op = "add";
                }
            case 8: //SUB
                if(!op) op = "sub";
            case 9: //AND
                if(!op) op = "and";
            case 10: //XOR
                if(!op) op = "xor";
            case 11: //OR
                if(!op) op = "or";
            case 12: //LAND
                if(!op) op = "and"; 
            case 13: //LOR
                if(!op) op = "or";
            case 16: //LE a<=b !a>b !b<a
                if(!op) op = "slt";
            case 17: //GE a>=b !a<b    
                if(!op) op = "slt";            
            case 18: //GT
                if(!op) op = "slt";
            case 19: //LT
                if(!op) op = "slt";               
                if (s == 18 || s == 16)
                {
                    s0 = interpret_load_reg(code[i].rec[2], "$s0");
                    s1 = interpret_load_reg(code[i].rec[1], "$s1");                
                }
                else
                { 
                    s0 = interpret_load_reg(code[i].rec[1], "$s0");
                    s1 = interpret_load_reg(code[i].rec[2], "$s1");
                }
                rflag = getreg(code[i].rec[0]);
                rec[0].type = MIPS_REG;
                rec[0].record.string = rflag >= 0 ? regs[rflag] : "$s0";
                rec[1].type = MIPS_REG;
                rec[1].record.string = s0;
                rec[2].type = MIPS_REG;
                rec[2].record.string = s1;
                mips_code_add("", op, 3, rec[0], rec[1], rec[2]);
                if (s == 16 || s == 17)
                {
                    rec[2].type = MIPS_NUMBER;
                    rec[2].record.integer = 1;
                    mips_code_add("", "sltiu", 3, rec[0], rec[0], rec[2]);
                }
                if (rflag < 0)
                    interpret_store(code[i].rec[0], "$s0");
                break;                
            case 14: //EQ
                s0 = interpret_load_reg(code[i].rec[1], "$s0");
                s1 = interpret_load_reg(code[i].rec[2], "$s1");
                rflag = getreg(code[i].rec[0]);
                rec[0].type = MIPS_REG;
                rec[0].record.string = rflag >= 0 ? regs[rflag] : "$s0";
                rec[1].type = MIPS_REG;
                rec[1].record.string = s0;
                rec[2].type = MIPS_REG;
                rec[2].record.string = s1;
                mips_code_add("", "xor", 3, rec[0], rec[1], rec[2]);
                rec[1].type = MIPS_NUMBER;
                rec[1].record.integer = 1;                
                mips_code_add("", "sltiu", 3, rec[0], rec[0], rec[1]);
                if (rflag < 0)
                    interpret_store(code[i].rec[0], "$s0");
                break;
            case 15: //NE
                s0 = interpret_load_reg(code[i].rec[1], "$s0");
                s1 = interpret_load_reg(code[i].rec[2], "$s1");
                rflag = getreg(code[i].rec[0]);
                rec[0].type = MIPS_REG;
                rec[0].record.string = rflag >= 0 ? regs[rflag] : "$s0";
                rec[1].type = MIPS_REG;
                rec[1].record.string = s0;
                rec[2].type = MIPS_REG;
                rec[2].record.string = s1;
                mips_code_add("", "xor", 3, rec[0], rec[1], rec[2]);
                rec[1].type = MIPS_REG;
                rec[1].record.string = "$0";                
                mips_code_add("", "sltu", 3, rec[0], rec[1], rec[0]);
                if (rflag < 0)
                    interpret_store(code[i].rec[0], "$s0");
                break;             
            case 20: //NOT
                s0 = interpret_load_reg(code[i].rec[1], "$s0");
                rflag = getreg(code[i].rec[0]);
                rec[0].type = MIPS_REG;
                rec[0].record.string = rflag >= 0 ? regs[rflag] : "$s0";
                rec[1].type = MIPS_REG;
                rec[1].record.string = s0;
                mips_code_add("", "not", 2, rec[0], rec[1]);
                if (rflag < 0)
                    interpret_store(code[i].rec[0], "$s0");
                break;
            case 21: //LNOT
                s0 = interpret_load_reg(code[i].rec[1], "$s0");
                rflag = getreg(code[i].rec[0]);
                rec[0].type = MIPS_REG;
                rec[0].record.string = s0;
                rec[1].type = MIPS_REG;
                rec[1].record.string = rflag >= 0 ? regs[rflag] : "$s0";   
                rec[2].type = MIPS_NUMBER;
                rec[2].record.integer = 1;
                mips_code_add("", "sltiu", 3, rec[1], rec[0], rec[2]);
                if (rflag < 0)
                    interpret_store(code[i].rec[0], "$s0");                
                break;
            case 22: //IF
                s0 = interpret_load_reg(code[i].rec[0], "$s0");
                rec[0].type = MIPS_REG;
                rec[0].record.string = s0;
                rec[1].type = MIPS_LABEL;
                sprintf(tmpstr, "l%d", code[i].rec[1].record);
                rec[1].record.string = strdup(tmpstr);
                mips_code_add("", "bnez", 2, rec[0], rec[1]);
                break;
            case 23: //CALL
                if (globalflag)
                {
                    freg[globalrec.record]=-2;
                    interpret_store(globalrec, regs[regN]);
                }
                if(code[i].rec[0].record == code[pmain].rec[0].record)
                {
                    sprintf(tmpstr, "main");
                }
                else if(code[i].rec[0].record == 0 && augcount==1) //printf("...");
                {
                    sprintf(tmpstr, "ls0");
                }
                else
                {
                    sprintf(tmpstr, "l%d", code[i].rec[0].record);
                }
                rec[0].type = MIPS_LABEL;
                rec[0].record.string = strdup(tmpstr);
                mips_code_add("", "jal", 1, rec[0]);
                if (globalflag)
                {
                    interpret_load(globalrec, regs[regN]);
                    freg[globalrec.record]=regN;                	
                }
                augcount = 0;
                break;
            case 24: //RET
            case 34: //END
                lregs(cureg);
                if (globalflag)
                {
                    freg[globalrec.record]=-2;
                    interpret_store(globalrec, regs[regN]);
                    if (s==24) freg[globalrec.record]=regN;
                }
                for(j=0; j<functionlen/32767; j++)
                {
                    sprintf(tmpstr, "%d", 32767);
                    rec[0].type = MIPS_REG;
                    rec[0].record.string = "$sp";
                    rec[1].type = MIPS_REG;
                    rec[1].record.string = "$sp";
                    rec[2].type = MIPS_REG;
                    rec[2].record.string = strdup(tmpstr);
                    mips_code_add("", "addiu", 3, rec[0], rec[1], rec[2]);
                }
                sprintf(tmpstr, "%d", functionlen % 32767);
                rec[0].type = MIPS_REG;
                rec[0].record.string = "$sp";
                rec[1].type = MIPS_REG;
                rec[1].record.string = "$sp";
                rec[2].type = MIPS_REG;
                rec[2].record.string = strdup(tmpstr);
                mips_code_add("", "addiu", 3, rec[0], rec[1], rec[2]);

                rec[0].type = MIPS_REG;
                rec[0].record.string = "$ra";
                mips_code_add("", "jr", 1, rec[0]);

                if(s == 34) level --;
                break;
            case 25: //PUSH
                rflag = getreg(code[i].rec[0]);
                interpret_load(code[i].rec[0], "$v0");
                break;
            case 26: //POP
                interpret_store(code[i].rec[0], "$v0");
                break;
            case 27: //TINT
            case 28: //TCHAR
            case 29: //MOVE
                if (level != 0 || code[i].rec[1].record != 0)
                {
                    if ((code[i].rec[0].type != IR_ADDRESS) || !(freg[code[i].rec[0].record] >= 0 && freg[code[i].rec[0].record] < regN) || (fleft[code[i].rec[0].record]<=i && i<=fright[code[i].rec[0].record]))
                    {
                        rflag = getreg(code[i].rec[0]);
                        if (code[i].rec[1].type == IR_CONST && rflag >= 0)
                        {
                            interpret_load(code[i].rec[1], regs[rflag]);
                        }
                        else
                        {
                            s0 = interpret_load_reg(code[i].rec[1], "$s0");
                            interpret_store(code[i].rec[0], s0);
                        }
                    }
                }
                break;
            case 30: //ADS
                interpret_load_ads(code[i].rec[1], "$s0");
                interpret_store(code[i].rec[0], "$s0");
                break;
            case 31: //STAR
                s0 = interpret_load_reg(code[i].rec[1], "$s0");
                rec[0].type = MIPS_REG;
                if(globalsymbol[code[i].rec[0].record] == -1)
                {
                    if(staticsymbol[code[i].rec[0].record] == -1)
                    {
                        interpret_sp_convert(functionsymbol[code[i].rec[0].record]);
                        rec[0].record.string = strdup(tmpstr);
                    }
                    else
                    {
                        interpret_gp_convert(staticsymbol[code[i].rec[0].record]);
                        rec[0].record.string = strdup(tmpstr);
                    }
                }
                else
                {
                    sprintf(tmpstr, "%d($gp)", globalsymbol[code[i].rec[0].record]);
                    rec[0].record.string = strdup(tmpstr);
                }
                rec[1].type = MIPS_REG;
                rec[1].record.string = s0;
                mips_code_add("", "sw", 2, rec[1], rec[0]);
                break;
            case 32: //NEW 不再考虑
                break;
            case 33: //FUNC
                gaugcount = 0;
                creg = 0;
                cureg = -1;
                if(code[i].rec[0].record == code[pmain].rec[0].record)
                {
                    sprintf(tmpstr, "main");
                }
                else
                {
                    sprintf(tmpstr, "l%d", code[i].rec[0].record);
                }
                label = strdup(tmpstr);
                mips_code_add(label, "", 0);
                functionlen = 0;
                memset(freg, -1, sizeof(freg));
                memset(regr, -1, sizeof(regr));
                memset(fleft, 11, sizeof(fleft));
                memset(fright, -1, sizeof(fright));
                memset(in, 0, sizeof(in));
                memset(out, 0, sizeof(out));
                memset(tin, 0, sizeof(tin));
                memset(tout, 0, sizeof(tout));
                memset(vreg, -1, sizeof(vreg));
                tin1 = tout1 = 0;
                vregn=0;
                globalflag = 0;
                for(j=i+1; j<codenum; j++)
                {
                    if(strcmp(code[j].op, "END") == 0) break;
                    if(strcmp(code[j].op, "ADS") == 0 && code[j].rec[1].type == IR_ADDRESS) freg[code[j].rec[1].record] = -2;
                    for(k=0; k<TTIFNUM; k++) if(strcmp(code[j].op, ttinstrformat[k].op) == 0) break;
                    for(k=ttinstrformat[k].num-1; k>=0; k--)
                    {
                        if(code[j].rec[k].type == IR_ADDRESS)
                        {
                            if (code[j].rec[k].flag != 4 || staticvar[code[j].rec[k].record] != -1)
                            {
                                freg[code[j].rec[k].record] = -2;
                            }
                            else if (globalvar[code[j].rec[k].record] != -1 && freg[code[j].rec[k].record] == -1)
                            {
                                freg[code[j].rec[k].record] = -2;
                                if (globalflag == 0 && !isstring[code[j].rec[k].record])
                                {
                                    globalflag = 1;
                                    globalrec=code[j].rec[k];
                                }
                            }
                        }
                    }
                }
                int fend = j;
                int flag = 1;
                while (flag && vregn < MAXALLOC)
                {
                    flag = 0;      
                    for (j=i+1;j<fend;j++)
                    {
                        int t = j-i-1;
                        memcpy(in1, in[t], tin[t]*sizeof(int));
                        memcpy(out1, out[t], tout[t]*sizeof(int));
                        tin1 = tin[t];
                        tout1 = tout[t];
                        int *adef = NULL, tdef = 0, *ause = NULL, tuse = 0, *ta = NULL, tt = 0;
                        tdef = def(j, &adef);
                        tuse = use(j, &ause);
                        tt = setminus(out[t], adef, tout[t], tdef, &ta);
                        free(in[t]);
                        in[t] = NULL;
                        tin[t] = setunion(ause, ta, tuse, tt, &in[t]);
                        free(ause);
                        free(ta);
                        free(adef);
                        if (strcmp(code[j].op, "IF") == 0)
                        {
                            free(out[t]);
                            int lb = findlb(code[j].rec[1].record, i+1);
                            tout[t] = setunion(in[t+1], in[lb], tin[t+1], tin[lb], &out[t]);
                        }
                        else if (strcmp(code[j].op, "GOTO") == 0)
                        {
                            free(out[t]);
                            int lb = findlb(code[j].rec[0].record, i+1);                		
                            tout[t] = tin[lb];    
                            if (tout[t])
                            {            		
                                out[t] = malloc(sizeof(int)*tout[t]);                			
                                memcpy(out[t], in[lb], tin[lb]*sizeof(int));
                            }
                            else
                                out[t] = NULL;
                        }
                        else
                        {
                            free(out[t]);
                            tout[t] = tin[t+1];    
                            if (tout[t])
                            {            	
                                out[t] = malloc(sizeof(int)*tout[t]);
                                memcpy(out[t], in[t+1], tin[t+1]*sizeof(int));
                            }
                            else
                                out[t] = NULL;
                        }
                        for (k=0;k<tin[t];++k)
                            if (j>fright[in[t][k]])
                            {
                                if (fright[in[t][k]]<0)
                                {
                                    vreg[vregn++] = in[t][k];
                                }
                                fright[in[t][k]] = j;
                            }
                        for (k=0;k<tout[t];++k)
                            if (j<fleft[out[t][k]]) fleft[out[t][k]] = j;
                        if (tout[t] != tout1 || tin[t] != tin1) flag=1;
                    }
                }
                if (vregn<MAXALLOC)
                {
                    for (j=vregn-1;j>=0;--j)
                        for (k=0;k<j;++k)
                            if (fleft[vreg[k]]>fleft[vreg[k+1]])
                            {
                                int tt = vreg[k];
                                vreg[k]=vreg[k+1];
                                vreg[k+1]=tt;
                            }
                    for (j=0;j<vregn;++j)
                    {
                        for(k=0;k<regN;++k)
                            if (regr[k]>=0 && fright[regr[k]]<fleft[vreg[j]])
                            {
                                creg--;
                                regr[k]=-1;
                            }
                        if (creg==regN)
                        {
                            int tt=-1;
                            for (k=0;k<regN;++k)
                                if (regr[k]>=0)
                                    if (tt<0||fright[regr[k]]>fright[regr[tt]])
                                        tt=k;
                            if (fright[regr[tt]]>fright[vreg[j]])
                            {
                                freg[regr[tt]]=-1;
                                regr[tt]=vreg[j];
                                freg[vreg[j]]=tt;
                            }
                        }
                        else
                        {
                            for (k=0;k<regN;++k) if (regr[k]<0) break;
                            regr[k]=vreg[j];
                            freg[vreg[j]]=k;
                            if (k>cureg) cureg = k;
                            creg++;
                        }
                    }
                }
                for (j=i+1; j < fend; ++j)
                {
                    for(k=0; k<TTIFNUM; k++) if(strcmp(code[j].op, ttinstrformat[k].op) == 0) break;
                    for(k=ttinstrformat[k].num-1; k>=0; k--)
                    {
                        if(code[j].rec[k].type == IR_ADDRESS)
                        {
                            if(globalvar[code[j].rec[k].record] == -1 &&
                                    staticvar[code[j].rec[k].record] == -1)
                            {
                                if (freg[code[j].rec[k].record] < 0)
                                {
                                    functionvar[code[j].rec[k].record] = functionlen;
                                    functionlen += code[j].rec[k].flag;
                                }
                            }
                        }
                        else if(code[j].rec[k].type == IR_SYMBOL)
                        {
                            if(globalsymbol[code[j].rec[k].record] == -1 &&
                                    staticsymbol[code[j].rec[k].record] == -1)
                            {
                                functionsymbol[code[j].rec[k].record] = functionlen;
                                functionlen += 4;
                            }
                        }
                        while(functionlen % 4) functionlen ++;
                    }
                }
                functionlen += 4*2 + 4 + 4*(cureg+1); //s0,s1,ra

                for(j=0; j<functionlen/32767; j++)
                {
                    sprintf(tmpstr, "%d", 32767);
                    rec[0].type = MIPS_REG;
                    rec[0].record.string = "$sp";
                    rec[1].type = MIPS_REG;
                    rec[1].record.string = "$sp";
                    rec[2].type = MIPS_REG;
                    rec[2].record.string = strdup(tmpstr);
                    mips_code_add("", "subu", 3, rec[0], rec[1], rec[2]);
                }
                sprintf(tmpstr, "%d", functionlen%32767);
                rec[0].type = MIPS_REG;
                rec[0].record.string = "$sp";
                rec[1].type = MIPS_REG;
                rec[1].record.string = "$sp";
                rec[2].type = MIPS_REG;
                rec[2].record.string = strdup(tmpstr);
                mips_code_add("", "subu", 3, rec[0], rec[1], rec[2]);

                sregs(cureg);

                if(code[i].rec[0].record == code[pmain].rec[0].record)
                {
                    maininsertpoint = mipscodenum - 1;
                }
                if (globalflag)
                {
                    interpret_load(globalrec, regs[regN]);
                    freg[globalrec.record] = regN;
                }
                level ++;
                break;
            case 35: //AUG
                s0 = interpret_load_reg(code[i].rec[0], "$s0");
                if(augcount < 4)
                {
                    sprintf(tmpstr, "$a%d", augcount);
                    rec[0].type = MIPS_REG;
                    rec[0].record.string = strdup(tmpstr);
                    rec[1].type = MIPS_REG;
                    rec[1].record.string = s0;
                    mips_code_add("", "move", 2, rec[0], rec[1]);
                }
                else
                {
                    NODE_IR_RECORD r;
                    r.type = IR_ADDRESS;
                    r.record = M-1-(augcount-4);
                    r.flag = 4;
                    interpret_store(r, s0);
                }
                augcount ++;
                break;
            case 36: //GAUG
                if(gaugcount < 4)
                {
                    sprintf(tmpstr, "$a%d", gaugcount);
                    interpret_store(code[i].rec[0], strdup(tmpstr));
                }
                else
                {
                    NODE_IR_RECORD r;
                    r.type = IR_ADDRESS;
                    r.record = M-1-(gaugcount-4);
                    r.flag = 4;
                    interpret_load(r, "$s0");
                    interpret_store(code[i].rec[0], "$s0");
                }
                gaugcount ++;
                break;
        }
    }
}
void output(int argc, char *argv[])
{
    FILE *out;
    FILE *in;
    int i, j, k;
    out = stdout;
    for(i=0; i<mipscodenum; i++)
    {
        if(i > 0 && strcmp(mipscode[i].op, "lw") == 0)
        {
            if(strcmp(mipscode[i-1].op, "sw") == 0 &&
                    mipscode[i-1].rec[0].type == MIPS_REG &&
                    mipscode[i].rec[0].type == MIPS_REG &&
                    strcmp(mipscode[i-1].rec[0].record.string, mipscode[i].rec[0].record.string) == 0 &&
                    strcmp(mipscode[i-1].rec[1].record.string, mipscode[i].rec[1].record.string) == 0)
            {
                continue;
            }
        }
        if(mipscode[i].label[0]) fprintf(out, "%s:\t", mipscode[i].label); else fprintf(out, "\t");
        if(mipscode[i].op[0])
        {
            fprintf(out, "%s\t", mipscode[i].op);
            for(j=0; j<mipscode[i].num; j++)
            {
                switch(mipscode[i].rec[j].type)
                {
                    case MIPS_REG:
                        fprintf(out, "%s", mipscode[i].rec[j].record.string);
                        break;
                    case MIPS_LABEL:
                        fprintf(out, "%s", mipscode[i].rec[j].record.string);
                        break;
                    case MIPS_NUMBER:
                        fprintf(out, "%d", mipscode[i].rec[j].record.integer);
                        break;
                    case MIPS_STRING:
                        fprintf(out, "%s", mipscode[i].rec[j].record.string);
                        break;
                }
                if(j+1<mipscode[i].num) fprintf(out, ",\t");
            }
        }
        fprintf(out, "\n");
        if(i == maininsertpoint)
        {
            for(j=M-1; j>mipscodemnum; j--)
            {
                if(j!=M-1 &&
                        strcmp(mipscode[j].op, "lw") == 0 &&
                        strcmp(mipscode[j+1].op, "sw") == 0 &&
                        mipscode[j+1].rec[0].type == MIPS_REG &&
                        mipscode[j].rec[0].type == MIPS_REG &&
                        strcmp(mipscode[j+1].rec[0].record.string, mipscode[j].rec[0].record.string) == 0 &&
                        strcmp(mipscode[j+1].rec[1].record.string, mipscode[j].rec[1].record.string) == 0)
                {
                    continue;
                }
                if(mipscode[j].label[0]) fprintf(out, "%s:\t", mipscode[j].label); else fprintf(out, "\t");
                if(mipscode[j].op[0])
                {
                    fprintf(out, "%s\t", mipscode[j].op);
                    for(k=0; k<mipscode[j].num; k++)
                    {
                        switch(mipscode[j].rec[k].type)
                        {
                            case MIPS_REG:
                                fprintf(out, "%s", mipscode[j].rec[k].record.string);
                                break;
                            case MIPS_LABEL:
                                fprintf(out, "%s", mipscode[j].rec[k].record.string);
                                break;
                            case MIPS_NUMBER:
                                fprintf(out, "%d", mipscode[j].rec[k].record.integer);
                                break;
                            case MIPS_STRING:
                                fprintf(out, "%s", mipscode[j].rec[k].record.string);
                                break;
                        }
                        if(k+1<mipscode[j].num) fprintf(out, ",\t");
                    }
                }
                fprintf(out, "\n");
            }
        }
    }

    fprintf(out, "ls0:\n");
    fprintf(out, "\tli\t$v0,\t4\n");
    fprintf(out, "\tsyscall\n");
    fprintf(out, "\tjr\t$ra\n");

    fprintf(out, "l2:\n");
    fprintf(out, "\tli\t$v0,\t9\n");
    fprintf(out, "\tsyscall\n");
    fprintf(out, "\tjr\t$ra\n");


    fprintf(out, "l1:\n");
    fprintf(out, "subu $sp, $sp, 4\n");
    fprintf(out, "sw $ra, 0($sp) \n");
    fprintf(out, "mcpy:\n");
    fprintf(out, "addi $a2, $a2, -1\n");
    fprintf(out, "lb $t0, 0($a1)\n");
    fprintf(out, "sb $t0, 0($a0)\n");
    fprintf(out, "addi $a0, $a0, 1\n");
    fprintf(out, "addi $a1, $a1, 1\n");
    fprintf(out, "bnez $a2, mcpy\n");
    fprintf(out, "lw $ra, 0($sp)\n");
    fprintf(out, "addu $sp, $sp, 4\n");
    fprintf(out, "jr $ra\n");
    fprintf(out, "l0:\n");
    fprintf(out, "subu $sp, $sp, 36\n");
    fprintf(out, "sw $ra, 32($sp) \n");
    fprintf(out, "sw $s0, 28($sp)\n");
    fprintf(out, "sw $s1, 24($sp)\n");
    fprintf(out, "sw $s2, 20($sp)\n");
    fprintf(out, "sw $s3, 16($sp)\n");
    fprintf(out, "sw $s4, 12($sp)\n");
    fprintf(out, "sw $s5, 8($sp)\n");
    fprintf(out, "sw $s6, 4($sp)\n");
    fprintf(out, "sw $t2, 0($sp)\n");
    fprintf(out, "move $s0, $a0 \n");
    fprintf(out, "move $s1, $a1\n");
    fprintf(out, "move $s2, $a2\n");
    fprintf(out, "move $s3, $a3\n");
    fprintf(out, "lw $t0, -4($gp)\n");
    fprintf(out, "lw $t1, -8($gp)\n");
    fprintf(out, "lw $t2, -12($gp)\n");
    fprintf(out, "li $s4, 0\n");
    fprintf(out, "la $s6, printf_buf\n");
    fprintf(out, "printf_loop:\n");
    fprintf(out, "lb $s5, 0($s0)\n");
    fprintf(out, "addu $s0, $s0, 1\n");
    fprintf(out, "beq $s5, '%', printf_fmt\n");
    fprintf(out, "beq $0, $s5, printf_end\n");
    fprintf(out, "printf_putc:\n");
    fprintf(out, "sb $s5, 0($s6)\n");
    fprintf(out, "sb $0, 1($s6)\n");
    fprintf(out, "move $a0, $s6\n");
    fprintf(out, "li $v0, 4 # print_str syscall\n");
    fprintf(out, "syscall\n");
    fprintf(out, "b printf_loop\n");
    fprintf(out, "printf_fmt:\n");
    fprintf(out, "lb $s5, 0($s0)\n");
    fprintf(out, "addu $s0, $s0, 1\n");
    fprintf(out, "beq $s4, 6, printf_loop\n");
    fprintf(out, "beq $s5, 'd', printf_int\n");
    fprintf(out, "beq $s5, 's', printf_str\n");
    fprintf(out, "beq $s5, 'c', printf_char\n");
    fprintf(out, "beq $s5, '0', printf_prefix\n");
    fprintf(out, "beq $s5, '.', printf_prefix\n");
    fprintf(out, "beq $s5, '%', printf_perc\n");
    fprintf(out, "b printf_loop\n");
    fprintf(out, "printf_shift_args:\n");
    fprintf(out, "move $s1, $s2\n");
    fprintf(out, "move $s2, $s3\n");
    fprintf(out, "move $s3, $t0\n");
    fprintf(out, "move $t0, $t1\n");
    fprintf(out, "move $t1, $t2\n");
    fprintf(out, "add $s4, $s4, 1\n");
    fprintf(out, "b printf_loop\n");
    fprintf(out, "printf_prefix:\n");
    fprintf(out, "lb $s5, 0($s0)\n");
    fprintf(out, "add $s0, $s0, 1\n");
    fprintf(out, "li $s7, 1\n");
    fprintf(out, "printf_prefix_loop_1:\n");
    fprintf(out, "mul $s7, $s7, 10\n");
    fprintf(out, "sub $s5, $s5, 1\n");
    fprintf(out, "bgt $s5, '1', printf_prefix_loop_1\n");
    fprintf(out, "printf_prefix_loop_2:\n");
    fprintf(out, "move $a0, $s1\n");
    fprintf(out, "div $a0, $a0, $s7\n");
    fprintf(out, "rem $a0, $a0, 10\n");
    fprintf(out, "li $v0, 1\n");
    fprintf(out, "syscall\n");
    fprintf(out, "div $s7, $s7, 10\n");
    fprintf(out, "bge $s7, 1, printf_prefix_loop_2\n");
    fprintf(out, "lb $s5, 0($s0)\n");
    fprintf(out, "addu $s0, $s0, 1\n");
    fprintf(out, "b printf_shift_args\n");
    fprintf(out, "printf_int:\n");
    fprintf(out, "move $a0, $s1\n");
    fprintf(out, "li $v0, 1\n");
    fprintf(out, "syscall\n");
    fprintf(out, "b printf_shift_args\n");
    fprintf(out, "printf_str:\n");
    fprintf(out, "move $a0, $s1\n");
    fprintf(out, "li $v0, 4\n");
    fprintf(out, "syscall\n");
    fprintf(out, "b printf_shift_args\n");
    fprintf(out, "printf_char:\n");
    fprintf(out, "sb $s1, 0($s6)\n");
    fprintf(out, "sb $0, 1($s6)\n");
    fprintf(out, "move $a0, $s6\n");
    fprintf(out, "li $v0, 4\n");
    fprintf(out, "syscall\n");
    fprintf(out, "b printf_shift_args\n");
    fprintf(out, "printf_perc:\n");
    fprintf(out, "li $s5, '%'\n");
    fprintf(out, "sb $s5, 0($s6)\n");
    fprintf(out, "sb $0, 1($s6)\n");
    fprintf(out, "move $a0, $s6\n");
    fprintf(out, "li $v0, 4\n");
    fprintf(out, "syscall\n");
    fprintf(out, "b printf_loop\n");
    fprintf(out, "printf_end:\n");
    fprintf(out, "lw $ra, 32($sp)\n");
    fprintf(out, "lw $s0, 28($sp)\n");
    fprintf(out, "lw $s1, 24($sp)\n");
    fprintf(out, "lw $s2, 20($sp)\n");
    fprintf(out, "lw $s3, 16($sp)\n");
    fprintf(out, "lw $s4, 12($sp)\n");
    fprintf(out, "lw $s5, 8($sp)\n");
    fprintf(out, "lw $s6, 4($sp)\n");
    fprintf(out, "lw $t2, 0($sp)\n");
    fprintf(out, "addu $sp, $sp, 36\n");
    fprintf(out, "jr $ra\n");
    fprintf(out, ".data 0x10020000\n");
    fprintf(out, "printf_buf: .space 4\n");

    fprintf(out, ".data 0x10008000\n");
    for(i=0; i<mipscodeheadnum; i++)
    {
        if(mipscodehead[i].label[0]) fprintf(out, "%s:\t", mipscodehead[i].label); else fprintf(out, "\t");
        if(mipscodehead[i].op[0])
        {
            fprintf(out, "%s\t", mipscodehead[i].op);
            for(j=0; j<mipscodehead[i].num; j++)
            {
                switch(mipscodehead[i].rec[j].type)
                {
                    case MIPS_REG:
                        fprintf(out, "%s", mipscodehead[i].rec[j].record.string);
                        break;
                    case MIPS_LABEL:
                        fprintf(out, "%s", mipscodehead[i].rec[j].record.string);
                        break;
                    case MIPS_NUMBER:
                        fprintf(out, "%d", mipscodehead[i].rec[j].record.integer);
                        break;
                    case MIPS_STRING:
                        fprintf(out, "%s", mipscodehead[i].rec[j].record.string);
                        break;
                }
                if(j+1<mipscodehead[i].num) fprintf(out, ",\t");
            }
        }
        fprintf(out, "\n");
    }

    fclose(out);
}
int main(int argc, char *argv[])
{
    init(argc, argv);
    interpret();
    output(argc, argv);
    return 0;
}
