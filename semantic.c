#include "semantic.h"

extern IR_RECORD IRmemcpy, IRmalloc;
IR_RECORD beginloop, endloop, logical_false;
int logflag = 0;

int calc_constexpr(AST_Node *node)
{
	if (node -> ntype == primary_expression1
	 || node -> ntype == primary_expression3
	 || node -> ntype == postfix_expression1
	 || node -> ntype == postfix_expression2
	 || node -> ntype == postfix_expression3
	 || node -> ntype == postfix_expression4
	 || node -> ntype == postfix_expression5
	 || node -> ntype == postfix_expression6
	 || node -> ntype == unary_expression1
	 || node -> ntype == unary_expression2
	 || node -> ntype == unary_expression5
	 || node -> ntype == unary_expression6)
	 	return 0;
	if (node -> ntype == expression)
	{
		/*
		int i;
		for (i = 0; i < nc(node)-1; ++i)
			calc_constexpr(node->child.list[i]);
		return calc_constexpr(cl(node));
		*/
		if (nc(node) > 1) return 0;
	}
	int isc = 1;
	int val = 0;
	int v1 = 0;
	int v2 = 0;
	if (node -> ntype == primary_expression2)
	{
		if (c1(node)->ntype == literal_node)
		{
			val = CValue(c1(node));
			isc = TN(c1(node)) == CHAR;
		}
		else return 1;
	}
	else if (node ->ntype == unary_expression3 || node ->ntype == unary_expression4)
	{
		semantic_check(c1(node));
		val = c1(node)->type->size;
	}
	else
	{
		if (!calc_constexpr(c1(node))) return 0;
		if (nc(node) > 1)
		{
			if (!calc_constexpr(c2(node))) return 0;
			v2 = reg(c2(node)).record;
			if (TN(c2(node)) == INT)
			{
				isc = 0;
			}
		}
		v1 = reg(c1(node)).record;
		if (TN(c1(node)) == INT)
		{
			isc = 0;
		}
		switch (node -> ntype)
		{
			case logical_or_expression : val = v1 || v2;break;
			case logical_and_expression : val = v1 && v2;break;
			case inclusive_or_expression : val = v1 | v2;break;
			case exclusive_or_expression : val = v1 ^ v2;break;
			case and_expression : val = v1 & v2;break;
			case equality_expression1 : val = v1 == v2;break;
			case equality_expression2 : val = v1 != v2;break;
			case relational_expression1: val = v1 < v2;break;
			case relational_expression2: val = v1 > v2;break;
			case relational_expression3: val = v1 <= v2;break;
			case relational_expression4: val = v1 >= v2;break;
			case shift_expression1: val = v1 << v2;break;
			case shift_expression2: val = v2 >> v2;break;
			case additive_expression1: val = v1 + v2;break;
			case additive_expression2: val = v1 - v2;break;
			case multiplicative_expression1 : val = v1 * v2;break;
			case multiplicative_expression2 : val = v1 / v2;break;
			case multiplicative_expression3 : val = v1 % v2;break;
			case unary_expression7 : val = v1;break;
			case unary_expression8 : val = -v1;break;
			case unary_expression9 : val = ~v1;break;
			case unary_expression10 : val = !v1;break;
			case primary_expression4 : val = v1;break;
			case expression : val = v1;break;
			default : return 0;
		}
	}
	node -> ntype = primary_expression2;
	node -> isl = 0;
	IR_RECORD rec = IR_new_const(val);
   	node -> ir = IR_makelist(IR_OR, 3, rec, rec, rec);
	if (isc)
	{
		node -> type = &char_type;
	}
	else
	{
		node -> type = &int_type;
   	}
	return 1;
}

void translate_expr(AST_Node *node)
{
	IR_OP op = 0;
	switch (node -> ntype)
	{
		case cast_expression:
		{
			if (TN(c1(node)) == INT && TN(c2(node)) == CHAR)
				node -> ir = IR_makelist(IR_TINT, 2, IR_new_address(4), reg(c2(node)));
			else if (TN(c2(node)) == INT && TN(c1(node)) == CHAR)
				node -> ir = IR_makelist(IR_TCHAR, 2, IR_new_address(1), reg(c2(node)));
			else
				node -> ir = c2(node)->ir;
			return;
		}
		case assignment_expression1:
		{
			if (reg(c1(node)).flag <= 4 && reg(c2(node)).flag <= 4)
			{
				node -> ir = IR_makelist(IR_MOVE, 2, reg(c1(node)), reg(c2(node)));
			}
			else
			{
				IR_RECORD r1 = IR_new_address(4);
				IR_RECORD r2 = IR_new_address(4);		
				IR_RECORD rc1 = reg(c1(node));
				node -> ir = IR_makelist(IR_ADS, 2, r1, rc1);
				node -> ir = IR_merge(2, node->ir, IR_makelist(IR_ADS, 2, r2, reg(c2(node))));						
				node -> ir = IR_merge(2, node->ir, IR_makelist(IR_AUG, 1, r1));
				node -> ir = IR_merge(2, node->ir, IR_makelist(IR_AUG, 1, r2));
				node -> ir = IR_merge(2, node->ir, IR_makelist(IR_AUG, 1, IR_new_const(c2(node)->type->size)));
				node -> ir = IR_merge(2, node->ir, IR_makelist(IR_CALL, 1, IRmemcpy));
				node -> ir = IR_merge(2, node->ir, IR_makelist(IR_OR, 3, rc1, rc1, rc1));
			}
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			return;
		}
		case assignment_expression2:
		case assignment_expression3:
		case assignment_expression4:
		case assignment_expression5:
		case assignment_expression6:
		case assignment_expression7:
		case assignment_expression8:
		case assignment_expression9:
		case assignment_expression10:
		case assignment_expression11:
		{
			switch (node -> ntype)
			{
				case assignment_expression2: op = IR_MUL;break;
				case assignment_expression3: op = IR_DIV;break;
				case assignment_expression4: op = IR_MOD;break;
				case assignment_expression5: op = IR_ADD;break;
				case assignment_expression6: op = IR_SUB;break;
				case assignment_expression7: op = IR_SHL;break;
				case assignment_expression8: op = IR_SHR;break;
				case assignment_expression9: op = IR_AND;break;
				case assignment_expression10: op = IR_XOR;break;
				case assignment_expression11: op = IR_OR;break;
				default: break;//impossible
			}
			node -> ir = IR_makelist(op, 3, reg(c1(node)), reg(c1(node)), reg(c2(node)));
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			return;
		}
		case postfix_expression1:
		{
			IR_RECORD r1;
			int t = TN(c1(node)) == ARRAY ? ARR(c1(node)->type)->type->size : PTR(c1(node)->type)->type->size;
			if (reg(c2(node)).type == IR_CONST)
			{
				r1 = IR_new_const(t * reg(c2(node)).record);
				node -> ir = IR_makelist(IR_OR, 3, r1, r1, r1);
			}
			else
			{
				r1 = IR_new_address(4);
				node -> ir = IR_makelist(IR_MUL, 3, r1, reg(c2(node)), IR_new_const(t));			
			}
			if (c1(node)->ntype == postfix_expression1 || TN(c1(node)) == POINTER)
			{
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADD, 3, IR_new_address(4), r1, reg(c1(node))));
			}
			else
			{
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADS, 2, IR_new_address(4), reg(c1(node))));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADD, 3, IR_new_address(4), r1, reg(node)));
			}
			if ((TN(c1(node)) == ARRAY && TN(ARR(c1(node)->type)) != ARRAY) ||
			    (TN(c1(node)) == POINTER && TN(PTR(c1(node)->type)) != ARRAY))
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_STAR, 2, IR_new_symbol(ARR(c1(node)->type)->type->size), reg(node)));
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			return;
		}		
		case postfix_expression2:
		{
			if (nc(node) > 1)
			{
				node -> ir = IR_merge(2, c2(node)->ir, IR_makelist(IR_CALL, 1, reg(c1(node))));
			}
			else
			{
				node -> ir = IR_merge(2, c1(node)->ir, IR_makelist(IR_CALL, 1, reg(c1(node))));
			}
			if (TN(node) != VOID)
			{
				if (TN(node) == STRUCT || TN(node) == UNION)
				{
					IR_RECORD rec1 = IR_new_address(4);
					IR_RECORD rec2 = IR_new_symbol(node -> type -> size);					
					node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_POP, 1, rec1));					
					node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_STAR, 2, rec2, rec1));										
				}
				else
					node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_POP, 1, IR_new_address(node -> type -> size)));
			}
			break;				
		}
		case postfix_expression3:
		{
			if (TN(c1(node)) == STRUCT)
			{
				int offset = calc_offset(ST(c1(node)->type), c2(node)->data);
				int size = find_scope(ST(c1(node)->type), c2(node)->data)->size;
				node -> ir = IR_makelist(IR_ADS, 2, IR_new_address(4), reg(c1(node)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADD, 3, IR_new_address(4), IR_new_const(offset), reg(node)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_STAR, 2, IR_new_symbol(size), reg(node)));
			}
			else
				node -> ir = c1(node)->ir;
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			return;
		}
		case postfix_expression4:
		{
			if (TN(ARR(c1(node)->type)) == STRUCT)
			{
				int offset = calc_offset(ST(PTR(c1(node)->type)->type), c2(node)->data);
				int size = find_scope(ST(PTR(c1(node)->type)->type), c2(node)->data)->size;
				node -> ir = IR_makelist(IR_ADD, 3, IR_new_address(4), IR_new_const(offset), reg(c1(node)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_STAR, 2, IR_new_symbol(size), reg(node)));
			}
			else
			{
				int size = find_scope(UN(PTR(c1(node)->type)->type), c2(node)->data)->size;
				node -> ir = IR_makelist(IR_STAR, 2, IR_new_symbol(size), reg(c1(node)));	
			}
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			return;
		}
		case additive_expression1:
		case additive_expression2:
		{
			op = node -> ntype == additive_expression1 ? IR_ADD : IR_SUB;
			if (TN(c1(node)) == POINTER)
			{
				node -> ir = IR_makelist(IR_MOVE, 2, IR_new_address(4), reg(c2(node)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_MUL, 3, IR_new_address(4), reg(node), IR_new_const(PTR(c1(node)->type)->type->size)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(op, 3, IR_new_address(4), reg(c1(node)), reg(node)));
			}
			else if (TN(c2(node)) == POINTER)
			{
				node -> ir = IR_makelist(IR_MOVE, 2, IR_new_address(4), reg(c1(node)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_MUL, 3, IR_new_address(4), reg(node), IR_new_const(PTR(c2(node)->type)->type->size)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(op, 3, IR_new_address(4), reg(c2(node)), reg(node)));
			}
			else if (TN(c1(node)) == ARRAY)
			{
				node -> ir = IR_makelist(IR_MOVE, 2, IR_new_address(4), reg(c2(node)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_MUL, 3, IR_new_address(4), reg(node), IR_new_const(ARR(c1(node)->type)->type->size)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(op, 3, IR_new_address(4), reg(c1(node)), reg(node)));
			}
			else if (TN(c2(node)) == ARRAY)
			{
				node -> ir = IR_makelist(IR_MOVE, 2, IR_new_address(4), reg(c1(node)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_MUL, 3, IR_new_address(4), reg(node), IR_new_const(ARR(c2(node)->type)->type->size)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(op, 3, IR_new_address(4), reg(c2(node)), reg(node)));
			}
			else
			{
				node -> ir = IR_makelist(op, 3, IR_new_address(4), reg(c1(node)), reg(c2(node)));//warning:cast
			}
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			return;
		}
		case unary_expression1:
		case unary_expression2:
		{
			node -> ir = IR_makelist(node -> ntype == unary_expression1 ? IR_ADD : IR_SUB, 3, reg(c1(node)), reg(c1(node)), IR_new_const(1));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case unary_expression3:
		case unary_expression4:
		{
			IR_RECORD rec = IR_new_const(c1(node)->type->size);
			node -> ir = IR_makelist(IR_OR, 3, rec, rec, rec);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case unary_expression5:
		{
			node -> ir = IR_makelist(IR_ADS, 2, IR_new_address(4), reg(c1(node)));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case unary_expression6:
		{
			if (TN(c1(node)) == ARRAY)
			{
				//node -> ir = IR_makelist(IR_OR, 3, reg(c1(node)), reg(c1(node)), reg(c1(node)));
				node -> ir = IR_makelist(IR_STAR, 2, IR_new_symbol(ARR(c1(node)->type)->type->size), reg(c1(node)));
			}
			else
			{
				node -> ir = IR_makelist(IR_STAR, 2, IR_new_symbol(PTR(c1(node)->type)->type->size), reg(c1(node)));
			}
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case unary_expression7 :
		{
			node -> ir = IR_makelist(IR_MOVE, 2, IR_new_address(4), reg(c1(node)));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case unary_expression8 :
		{
			node -> ir = IR_makelist(IR_SUB, 3, IR_new_address(4), IR_new_const(0), reg(c1(node)));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case unary_expression9 :
		{
			node -> ir = IR_makelist(IR_NOT, 2, IR_new_address(4), reg(c1(node)));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case unary_expression10 :
		{
			node -> ir = IR_makelist(IR_LNOT, 2, IR_new_address(4), reg(c1(node)));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case postfix_expression5:
		{
			IR_RECORD rec = IR_new_address(c1(node)->type->size);
			node -> ir = IR_makelist(IR_MOVE, 2, rec, reg(c1(node)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADD, 3, reg(c1(node)), reg(c1(node)), IR_new_const(1)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_OR, 3, rec, rec, rec));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case postfix_expression6:
		{
			IR_RECORD rec = IR_new_address(c1(node)->type->size);
			node -> ir = IR_makelist(IR_MOVE, 2, rec, reg(c1(node)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_SUB, 3, reg(c1(node)), reg(c1(node)), IR_new_const(1)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_OR, 3, rec, rec, rec));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			break;
		}
		case logical_or_expression:
		{
/*
a || b

a
if(a) goto l1
b
val = b
goto l2
l1: 
val = 1
l2:
exp = val
*/
			IR_RECORD l1 = IR_new_label();
			IR_RECORD l2 = IR_new_label();			
			IR_RECORD val = IR_new_address(4);
			node -> ir = IR_makelist(IR_IF, 2, reg(c1(node)), l1);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			node -> ir = IR_merge(3, node -> ir, c2(node)->ir, IR_makelist(IR_MOVE, 2, val, reg(c2(node))));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_GOTO, 1, l2));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, l1));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_MOVE, 2, val, IR_new_const(1)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, l2));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_OR, 3, val, val, val));			
			return;
		}
		case logical_and_expression:
		{
/*
a && b

a
if(!a) goto false
b
exp=b
goto end
false: 
exp=0
end:

a && b && c

a
if(!a) goto FALSE
b
exp=b
if(!exp) goto FALSE
c
exp=c
goto END
FALSE:
exp=0
END:
*/

			IR_RECORD aa = reg(c1(node));
			IR_RECORD bb = reg(c2(node));
			IR_RECORD na = IR_new_address(4);
			node -> ir = IR_makelist(IR_LNOT, 2, na, aa);
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_IF, 2, na, logical_false));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			node -> ir = IR_merge(2, node -> ir, c2(node) -> ir);
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_OR, 3, bb, bb, bb));			
			return;
		}
		default:
		{
			switch (node -> ntype)
			{
				case inclusive_or_expression : op = IR_OR;break;
				case exclusive_or_expression : op = IR_XOR;break;
				case and_expression : op = IR_AND;break;
				case equality_expression1 : op = IR_EQ;break;
				case equality_expression2 : op = IR_NE;break;
				case relational_expression1 : op = IR_LT;break;
				case relational_expression2 : op = IR_GT;break;
				case relational_expression3 : op = IR_LE;break;
				case relational_expression4 : op = IR_GE;break;
				case shift_expression1 : op = IR_SHL;break;
				case shift_expression2 : op = IR_SHR;break;
				case multiplicative_expression1 : op = IR_MUL;break;
				case multiplicative_expression2 : op = IR_DIV;break;
				case multiplicative_expression3 : op = IR_MOD;break;
				default : printf("%d\n", node -> ntype);printerr("WTFFF?!");
			}
			node -> ir = IR_makelist(op, 3, IR_new_address(4), reg(c1(node)), reg(c2(node)));	//warning
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
		}
	}
}

void semantic_check_struct_union(AST_Node *node)
{
#ifdef DEBUG
	printf("su%d\n", node -> ntype);
	fflush(stdout);
#endif
    if (node -> ntype == type_specifier6)
    {
    	semantic_check(c1(node));
        char *id = (char*)c2(node)->data;
        Type *t = look_type(id);
        if (t)
        { 
        	if (t->type_class != c1(node)->type->type_class)
        		printerr("struct/union tag conflict");
        }
      	else
        {
        	t = c1(node)->type;
	    	add_type(id, t);
	    }
	    node -> type = t;
    }
    else
    {
    	semantic_check(c1(node));
        Type *type = c1(node)->type;
        type -> size = 0;
        ST(type)->field_cnt = 0;        
        int i;
        for (i = 0; i < nc(cl(node)); ++i)
        {
            AST_Node *ch = cl(node)->child.list[i];
            semantic_check(c1(ch));
            ST(type)->field_cnt += nc(c2(ch));
        }
        ST(type)->field = checked_malloc(sizeof(Type *) * ST(type)->field_cnt);
        ST(type)->field_name = checked_malloc(sizeof(char *) * ST(type)->field_cnt);
		int cnt = 0;
        for (i = 0; i < nc(cl(node)); ++i)
        {
            AST_Node *ch = cl(node)->child.list[i];
            int j;
            for (j = 0; j < nc(c2(ch)); ++j)
            {
            	Type *ty = c1(ch)->type;
                AST_Node *cch = c2(ch)->child.list[j];
                if (cch->ntype == declarator1 || cch->ntype == declarator2)
                    printerr("function def in struct/union");
                semantic_check_declarator_array(cch, ty, 0);
				while (cch->ntype == declarator_array) cch = c1(cch);			
				ty = cch -> type;
                char *id = get_declarator_id(cch);
                int k;
                for (k = 0; k < cnt; ++k)
			    	if (strcmp(id, ST(type)->field_name[k])==0)
    	    			printerr("Identifier conflict");
                if (ty->type_class == VOID)
                    printerr("Cannot define a void var");
                ST(type)->field[cnt] = ty;
                ST(type)->field_name[cnt++] = id;
                if (type->type_class == STRUCT)
	                type -> size += align(ty -> size);
	            else if (ty -> size > type -> size)
	            	type -> size = align(ty -> size);
            }
        }
        if (node -> ntype == type_specifier4)
        {
	        char *id = (char*)c2(node)->data;
    	    Type *t = look_type_top(id);
    	    if (t)
    	    {
    	    	 if (t->type_class != c1(node)->type->type_class || (ST(t)->field_cnt != 0 && !same_type(t, type)))
		            printerr("struct/union tag conflict");
		    }
    	    if (!t)
		        add_type(id, type);
		    if (t && ST(t)->field_cnt == 0)
		    {
		    	def_type(id, type);
		    	type = t;
		    }
		}
	    node -> type = type;
    }
}

void semantic_check_plain_declaration(AST_Node *node) //ok
{
#ifdef DEBUG
	printf("pd%d\n", node -> ntype);
	fflush(stdout);
#endif
    semantic_check_declarator(c2(node), c1(node)->type, 1);
    if(TN(c2(node)) == FUNCTION) //函数
    {
        printerr("function para error");
    }
    else //变量
    {
   		AST_Node *ch = c2(node);
		while (ch->ntype == declarator_array) ch = c1(ch);		
        node -> type = ch->type;
        node -> ir = c2(node)->ir;
    }
}

void semantic_check_declarator_array(AST_Node *node, Type *type, int flag) //ok
{
    //变量数组定义
    if (node -> ntype != declarator_array)
    	semantic_check_plain_declarator(node, type);
    else
    {
    	AST_Node *t1[100], *ch = node;
    	int t = 0, t2[100];
		while (ch -> ntype == declarator_array)
		{
			if (!calc_constexpr(c2(ch)))
				printerr("In a[b], b must be a const");
			t1[t] = ch;
			t2[t++] = reg(c2(ch)).record;
			ch = c1(ch);
		}
		semantic_check_plain_declarator(ch, type);
		t1[0]->type = ch -> type;
		int i;
		for (i = 1; i < t; ++i)
		{
			t1[i]->type = array_of(t1[i-1]->type, t2[i-1]);
		}
		if (flag)
			c1(t1[t-1])->type = pointer_to(t1[i-1]->type);
		else
			c1(t1[t-1])->type = array_of(t1[i-1]->type, t2[i-1]);
	}
}

void semantic_check_plain_declarator(AST_Node *node, Type *type) //ok
{
    //ptr
    if (node -> ntype != plain_declarator)
    	semantic_check_identifier_node(node, type);
    else
    {
   		char *id = (char*)c2(node)->data;
	    if (look_var_top(id))
    	    printerr("Identifier conflict"); 
	    semantic_check(c1(node));
    	node -> type = wrap_type(type, c1(node)->type);
    }
}

void semantic_check_identifier_node(AST_Node *node, Type *type)  //ok
{
    char *id = (char*)node -> data;
    if (look_var_top(id))
        printerr("Identifier conflict");
    if (type->type_class == VOID)
        printerr("Cannot define a void var");
    node -> type = type;
}
char *get_declarator_id(AST_Node *node) //ok
{
		while (node -> ntype == declarator_array)
			node = c1(node);
		if (node -> ntype == plain_declarator)
			node = c2(node);
		return (char*)node -> data;
}
void semantic_check_declarator(AST_Node *node, Type *type, int flag) //ok
{
#ifdef DEBUG
	printf("dor%d\n", node -> ntype);
	fflush(stdout);
#endif
    if (node -> ntype == declarator1 || node -> ntype == declarator2)
    {
        Type *returntype = NULL;
        char *id = NULL;
        int arg_count = 0;
        Type **arg = NULL;
        if (c1(node)->ntype == plain_declarator)
        {
            id = (char*)c2(c1(node))->data;
            returntype = new_pointer_type(type, (int*)c1(c1(node))->data);
        }
        else
        {
            id = (char*)c1(node)->data;
            returntype = type;
        }
        if (node -> ntype == declarator1)
        {
            semantic_check(c2(node));
            arg_count = nc(c2(node));
            arg = malloc(sizeof(Type*) * arg_count);
            int i;
            for(i = 0; i < arg_count; ++i)
                arg[i] = (c2(node)->child.list[i])->type;
        }
        Type *fun = func_decl(returntype, arg, arg_count);
        Type *t = look_var_top(id);
        if (!t)
        {
        	node -> ir = IR_makelist(IR_LAB, 1, add_var(id, fun));
        }
        else if (!same_type(t, fun))
            printerr("Function declaration conflict");
        node -> type = fun;
    }
    else if (node -> ntype == declarator_array)
    {
		semantic_check_declarator_array(node, type, flag);
		AST_Node *ch = node;
		while (ch->ntype == declarator_array) ch = c1(ch);			
		IR_RECORD rec = add_var(get_declarator_id(node), ch -> type);
		node -> ir = IR_makelist(IR_OR, 3, rec, rec, rec);
    }
    else if (node -> ntype == plain_declarator)
    {
		semantic_check_plain_declarator(node, type);
		IR_RECORD rec = add_var((char*)c2(node)->data, node -> type);
		node -> ir = IR_makelist(IR_OR, 3, rec, rec, rec);
    }
    else //identifier_node
    {
		semantic_check_identifier_node(node, type);
		IR_RECORD rec;
		if (flag && (TN(node) == STRUCT || TN(node) == UNION))
			rec = add_var_sym((char*)node -> data, node -> type);
		else
			rec = add_var((char*)node -> data, node -> type);
		node -> ir = IR_makelist(IR_OR, 3, rec, rec, rec);
    }
}

void semantic_check_init_declarator(AST_Node *node, Type *type) //ok
{
#ifdef DEBUG
	printf("id%d\n", node -> ntype);
	fflush(stdout);
#endif
    if (node -> ntype == init_declarator)
    {
        semantic_check_declarator(c1(node), type, 0);
        if (c2(node)->ntype == initializer_list)
        {
        	if (TN(c1(node)) != ARRAY)
        		printerr("a = {...}, a must be an array");
		    int i;
		    IR_RECORD r1 = IR_new_address(4);
		    IR_RECORD r2 = IR_new_symbol(ARR(c1(node)->type)->type->size);
		    c2(node)->ir = IR_makelist(IR_ADS, 2, r1, reg(c1(node)));
  			for (i = 0; i < nc(c2(node)); ++i)
  			{
     		   semantic_check(c2(node) -> child.list[i]);
     		   c2(node)->ir = IR_merge(2, c2(node)->ir, IR_makelist(IR_STAR, 2, r2, r1));     		   
     		   c2(node)->ir = IR_merge(2, c2(node)->ir, c2(node) -> child.list[i]->ir);
     		   c2(node)->ir = IR_merge(2, c2(node)->ir, IR_makelist(IR_MOVE, 2, r2, reg(c2(node))));
     		   if (i < ARR(c1(node)->type)->capacity-1)
	     		   c2(node)->ir = IR_merge(2, c2(node)->ir, IR_makelist(IR_ADD, 3, r1, r1, IR_new_const(4)));
     		}
  			for (; i < ARR(c1(node)->type)->capacity; ++i)
  			{
     		   c2(node)->ir = IR_merge(2, c2(node)->ir, IR_makelist(IR_STAR, 2, r2, r1));     		   
     		   c2(node)->ir = IR_merge(2, c2(node)->ir, IR_makelist(IR_MOVE, 2, r2, IR_new_const(0)));
     		   if (i < ARR(c1(node)->type)->capacity-1)
	     		   c2(node)->ir = IR_merge(2, c2(node)->ir, IR_makelist(IR_ADD, 3, r1, r1, IR_new_const(4)));
     		}     		
        }
        else
        {
        	semantic_check(c2(node));
	        //判读初始化值是否合法???
	        if (scope == 1)
	        {
	        	node -> ir = IR_makelist(IR_OR, 3, reg(c1(node)), reg(c1(node)), reg(c1(node)));
	        	globalIR = IR_merge(2, globalIR, IR_makelist(IR_MOVE, 2, reg(c1(node)), reg(c2(node))));
	        }
	        else
	        	node -> ir = IR_makelist(IR_MOVE, 2, reg(c1(node)), reg(c2(node)));
    	}
        node -> ir = IR_merge(2, c2(node) -> ir, node -> ir);
        node -> ir = IR_merge(2, c1(node) -> ir, node -> ir);
    }
    else
    {
        semantic_check_declarator(node, type, 0);
        if (scope == 1)
        {
        	globalIR = IR_merge(2, globalIR, IR_makelist(IR_MOVE, 2, reg(node), IR_new_const(0)));
        	node -> ir = IR_makelist(IR_OR, 3, reg(node), reg(node), reg(node));
        }
    }
    
}
void semantic_check_declaration(AST_Node *node)
{
#ifdef DEBUG
	printf("dc%d\n", node -> ntype);
	fflush(stdout);
#endif
    int i;
    for(i = 0; i < nc(c2(node)); ++i)
    {
        semantic_check_init_declarator(c2(node)->child.list[i], c1(node)->type);
        node -> ir = IR_merge(2, node -> ir, c2(node)->child.list[i]->ir);
    }
}
void semantic_check_function_definition(AST_Node *node)
{
#ifdef DEBUG
	printf("def%d\n", node -> ntype);
	fflush(stdout);
#endif
    Type *returntype = NULL;
    char *id = NULL;
    int arg_count = 0;
    Type **arg = NULL;
    if (c2(node)->ntype == plain_declarator)
    {
    	semantic_check(c1(c2(node)));
        id = (char*)c2(c2(node))->data;
        returntype = wrap_type(c1(node)->type, c1(c2(node))->type);
    }
    else
    {
        id = (char*)c2(node)->data;
        returntype = c1(node)->type;
    }
    if (node -> ntype == function_definition1)
    {
        arg_count = nc(c3(node));
        arg = malloc(sizeof(Type*) * arg_count);
        int i;
        for(i = 0; i < arg_count; ++i)
            arg[i] = (c3(node)->child.list[i])->type;
    }
    if (returntype->type_class == STRUCT || returntype->type_class == UNION)
    	if (ST(returntype)->field_cnt == 0)
    		printerr("return type must be defined");
    Type *fun = func_decl(returntype, arg, arg_count);
    Type *t = look_var_top(id);
    if (t)
    {
    	if (!same_type(fun, look_var_top(id)))
       		printerr("Function definition confilict");
    }
    else
    {
    	node -> ir = IR_makelist(IR_FUNC, 1, add_var(id, fun));
    }
    node -> type = fun;
}
void semantic_check(AST_Node *node)
{
#ifdef DEBUG
	printf("%d\n", node -> ntype);
	fflush(stdout);
#endif
    Type *tret = NULL;
    switch (node -> ntype)
    {
        case program:
        {
            int i;
   			for (i = 0; i < nc(node); ++i)
   			{
		        semantic_check(node -> child.list[i]);
		        node -> ir = IR_merge(2, node -> ir, node -> child.list[i]->ir);
		    }
		    if (!mainIR)
		    	printerr("main function not found");
		    node -> ir = IR_merge(3, globalIR, mainIR, node -> ir);
        	break;
        }
        case declaration1:
        {
            semantic_check(c1(node));
            semantic_check_declaration(node);
            break;
        }
        case declaration2:
        {
            semantic_check(c1(node));
            break;
        }
        case function_definition1:
        {
            semantic_check(c1(node));
            begin_scope();
            semantic_check(c3(node));
            hold_end_scope();
            semantic_check_function_definition(node);
            hold_begin_scope();
            tret = nowret;
            nowret = FUN(node -> type) -> ret;
            semantic_check(c4(node));
            nowret = tret;
            end_scope();
            node -> ir = IR_merge(4, node -> ir, c3(node)->ir, cl(node) -> ir, IR_makelist(IR_END, 0));
            break;
        }
        case function_definition2:
        {
            semantic_check(c1(node));
            semantic_check_function_definition(node);
            begin_scope();
            tret = nowret;
            nowret = FUN(node -> type) -> ret;
            semantic_check(c3(node));
            nowret = tret;
            end_scope();     
            node -> ir = IR_merge(3, node -> ir, cl(node) -> ir, IR_makelist(IR_END, 0));
            break;
        }
        case parameters:
        {
            int i;
            IR *t = NULL;
		    for (i = 0; i < nc(node); ++i)
		    {
		        semantic_check(c1(node -> child.list[i]));	
            	semantic_check_plain_declaration(node -> child.list[i]);
            	if (TN(node -> child.list[i]) == STRUCT || TN(node -> child.list[i]) == UNION)
            	{
            		IR_RECORD rec = IR_new_address(4);
            		node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_GAUG, 1, rec));
            		t = IR_merge(2, t, IR_makelist(IR_STAR, 2, reg(node -> child.list[i]), rec));
            	}
            	else
		        	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_GAUG, 1, reg(node -> child.list[i])));
		    }
		    node -> ir = IR_merge(2, node -> ir, t);
            break;
        }
        case declarator_list:
        case init_declarator_list:
        case init_declarator:
        case declarator1:
        case declarator2:
        case declarator_array:
        case plain_declarator:
        case plain_declaration:
        {
            //不可能
            printerr("WTF");
            break;
        }
        case pointer:
        {
            semantic_check(c1(node));
            node -> type = pointer_to(c1(node)->type);
            break;
        }
        case type_specifier1:
        {
            node -> type = &void_type;
            break;
        }
        case type_specifier2:
        {
            node -> type = &char_type;
            break;
        }
        case type_specifier3:
        {
            node -> type = &int_type;
            break;
        }
        case type_specifier5:
        case type_specifier4:
        case type_specifier6:
        {
            semantic_check_struct_union(node);
            break;
        }
        case type_declaration_list:
        case type_declaration:
        {
            break;
        }
        case struct_or_union1:
        {
        	node -> type = new_struct_type();
        	break;
        }
        case struct_or_union2:
        {
        	node -> type = new_union_type();
            break;
        }
        case statement:
        {
        	node -> isl = 0;
            if (c1(node)->ntype == compound_statement)
            {
                if (nc(c1(node)))
                {
                    begin_scope();
                    semantic_check(c1(node));
                    end_scope();
                    node -> ir = c1(node) -> ir;
                }
            }
            else
            {
                semantic_check(c1(node));
                node -> ir = c1(node) -> ir;
            }
            break;
        }
        case compound_statement:
        {
        	node -> isl = 0;
            if (nc(node))
            {
                semantic_check(c1(node));
                node -> ir = c1(node) -> ir;
            }
            break;
        }
        case block_item_list:
        {
            int i;
		    for (i = 0; i < nc(node); ++i)
		    {
        		semantic_check(node -> child.list[i]);
        		node -> ir = IR_merge(2, node -> ir, node -> child.list[i] -> ir);
        	}
            break;
        }
        case expression_statement:
        {
        	node -> type = &int_type;
            break;
        }
        case selection_statement1:
        {
            semantic_check(node -> child.list[0]);
            if (!is_scalar(TN(c1(node)))) 
                printerr("If condition error");
            semantic_check(c2(node));
            semantic_check(c3(node));
/*
if(x) A else B

if(x) goto l1;
B
goto l2;
l1:
A
l2:
*/
            IR_RECORD l1 = IR_new_label();
            IR_RECORD l2 = IR_new_label();
            node -> ir = IR_makelist(IR_IF, 2, reg(c1(node)), l1);
            node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
            node -> ir = IR_merge(2, node -> ir, c3(node)->ir);
            node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_GOTO, 1, l2));
            node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, l1));
            node -> ir = IR_merge(2, node -> ir, c2(node)->ir);
            node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, l2));
            break;
        }
        case selection_statement2:
        {
            semantic_check(c1(node));
            if (!is_scalar(TN(c1(node)))) 
                printerr("If condition error");
            semantic_check(c2(node));
/*
if(x) A

if(!x) goto l1;
A
l1:
*/
			IR_RECORD l1 = IR_new_label();
			node -> ir = IR_makelist(IR_LNOT, 2, IR_new_address(4), reg(c1(node)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_IF, 2, reg(node), l1));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			node -> ir = IR_merge(2, node -> ir, c2(node)->ir);
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, l1));
            break;
        }
        case iteration_statement1:
        {
        	++loop;
        	IR_RECORD obw = beginloop, oew = endloop;
        	beginloop = IR_new_label();
        	endloop = IR_new_label();
        	semantic_check(c1(node));
        	if (TN(c1(node)) != INT && TN(c1(node)) != CHAR && TN(c1(node)) != POINTER) 
        		printerr("While condition error");
        	semantic_check(c2(node));
			node -> ir = IR_makelist(IR_LNOT, 2, IR_new_address(4), reg(c1(node)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_IF, 2, reg(node), endloop));
			node -> ir = IR_merge(2, node -> ir, c2(node)->ir);
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_GOTO, 1, beginloop));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, endloop));
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
			node -> ir = IR_merge(2, IR_makelist(IR_LAB, 1, beginloop), node -> ir);
        	beginloop = obw;
        	endloop = oew;
        	--loop;
        	break;
        }
        case iteration_statement2:
        {
        	++loop;
        	IR_RECORD obw = beginloop, oew = endloop;
        	beginloop = IR_new_label();
        	endloop = IR_new_label();
        	semantic_check(c1(node));
        	if (TN(c1(node)) != INT && TN(c1(node)) != CHAR && TN(c1(node)) != POINTER) 
        		printerr("For condition error");
        	semantic_check(c2(node));
        	if (TN(c2(node)) != INT && TN(c2(node)) != CHAR && TN(c2(node)) != POINTER) 
        		printerr("For condition error");
        	semantic_check(c3(node));
			node -> ir = IR_makelist(IR_LNOT, 2, IR_new_address(4), reg(c2(node)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_IF, 2, reg(node), endloop));
			node -> ir = IR_merge(2, node -> ir, c3(node)->ir);
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_GOTO, 1, beginloop));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, endloop));
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, IR_makelist(IR_LAB, 1, beginloop), node -> ir);			
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
        	beginloop = obw;
        	endloop = oew;
        	--loop;
        	break;        	
        }
        case iteration_statement3:
        {
        	++loop;
        	IR_RECORD obw = beginloop, oew = endloop;
        	beginloop = IR_new_label();
        	endloop = IR_new_label();
        	semantic_check(c1(node));
        	if (TN(c1(node)) != INT && TN(c1(node)) != CHAR && TN(c1(node)) != POINTER) 
        		printerr("For condition error");
        	semantic_check(c2(node));
        	if (TN(c2(node)) != INT && TN(c2(node)) != CHAR && TN(c2(node)) != POINTER) 
        		printerr("For condition error");
        	semantic_check(c3(node));
        	if (TN(c3(node)) != INT && TN(c3(node)) != CHAR && TN(c3(node)) != POINTER) 
        		printerr("For condition error");
        	semantic_check(c4(node));
			node -> ir = IR_makelist(IR_LNOT, 2, IR_new_address(4), reg(c2(node)));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_IF, 2, reg(node), endloop));
			node -> ir = IR_merge(2, node -> ir, c4(node)->ir);
			node -> ir = IR_merge(2, node -> ir, c3(node)->ir);
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_GOTO, 1, beginloop));
			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, endloop));
			node -> ir = IR_merge(2, c2(node)->ir, node -> ir);
			node -> ir = IR_merge(2, IR_makelist(IR_LAB, 1, beginloop), node -> ir);			
			node -> ir = IR_merge(2, c1(node)->ir, node -> ir);
        	beginloop = obw;
        	endloop = oew;
        	--loop;
        	break;  
        }
        case jump_statement1:
        {
            if (!loop)
                printerr("Continue must in a loop!");
            node -> ir = IR_makelist(IR_GOTO, 1, beginloop);
            break;
        }
        case jump_statement2:
        {
            if (!loop)
                printerr("Break must in a loop!");
            node -> ir = IR_makelist(IR_GOTO, 1, endloop);
            break;
        }
        case jump_statement3:
        {
        	node -> ir = IR_makelist(IR_RET, 0);
            break;
        }
        case jump_statement4:
        {
            semantic_check(c1(node));
            if (!same_type_cast(nowret, c1(node)->type))
                printerr("return type conflict");
            if (nowret->type_class == STRUCT || nowret->type_class == UNION)
            {
            	IR_RECORD rec1 = IR_new_address(4);
            	IR_RECORD rec2 = reg(c1(node));
            	IR_RECORD rec3 = IR_new_address(4);
            	node -> ir = IR_merge(2, c1(node) -> ir, IR_makelist(IR_AUG, 1, IR_new_const(nowret->size)));
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_CALL, 1, IRmalloc));
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_POP, 1, rec1));
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADS, 2, rec3, rec2));
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_AUG, 1, rec1));            	
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_AUG, 1, rec3));
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_AUG, 1, IR_new_const(nowret->size)));            	
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_CALL, 1, IRmemcpy));            	
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_PUSH, 1, rec1));
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_RET, 0));
            }
            else 
            	node -> ir = IR_merge(3, c1(node)->ir, IR_makelist(IR_PUSH, 1, reg(c1(node))), IR_makelist(IR_RET, 0));
            break;
        }
        case primary_expression1:
        {
            node -> type = look_var((char*)c1(node)->data);
            if (!node -> type)
                printerr("ID Not found!");
            node -> isl = (node -> type->type_class != ARRAY);
            node -> ir = IR_makelist(IR_OR, 3, recbuf, recbuf, recbuf);
            break;
        }
        case primary_expression2:
        {
        	if (c1(node)->ntype == literal_node)
        	{
	            node -> type = c1(node)->type;
    	        node -> isl = 0;
    	        IR_RECORD rec = IR_new_const(CValue(c1(node)));
    	       	node -> ir = IR_makelist(IR_OR, 3, rec, rec, rec);
    	    }
            break;
        }
        case primary_expression3:
        {
            node -> type = c1(node)->type;
            node -> isl = 0;
            if (scope == 1)
            {
            	IR_RECORD rec1 = IR_new_address(4);
            	IR_RECORD rec2 = IR_new_address(4);
            	globalIR = IR_merge(2, globalIR, IR_makelist(IR_MOVE, 2, rec1, IR_new_string(c1(node)->data)));
            	globalIR = IR_merge(2, globalIR, IR_makelist(IR_ADS, 2, rec2, rec1));
            	node -> ir = IR_makelist(IR_OR, 3, rec2, rec2, rec2);
            }
            else
            {
            	node -> ir = IR_makelist(IR_MOVE, 2, IR_new_address(4), IR_new_string(c1(node)->data));
            	node -> ir = IR_merge(2, node->ir, IR_makelist(IR_ADS, 2, IR_new_address(4), reg(node)));
            }
            break;
        }
        case primary_expression4:
        {
            semantic_check(c1(node));
            node -> type = c1(node)->type;
            node -> isl = c1(node)->isl;
            node -> ir = c1(node)->ir;
            break;
        }
        case postfix_expression1:
        {
    		semantic_check(c1(node));
		    semantic_check(c2(node));
		    if (TN(c2(node)) == INT || TN(c2(node)) == CHAR)
		    {
		         if (TN(c1(node)) == ARRAY)
        		     node -> type = ARR(c1(node)->type)->type;
        		 else if (TN(c1(node)) == POINTER)
        		     node -> type = PTR(c1(node)->type)->type;
        		 else
        		     printerr("in a[b], a must be a pointer/array");
     		}
		    else
        		printerr("in a[b], b must be a int/char");
		    node -> isl = 1;
		    translate_expr(node);
            break;
        }
        case postfix_expression2:
        {
            semantic_check(c1(node));
            if (TN(c1(node)) != FUNCTION)
                printerr("in a(...), a must be a function");
            if (nc(node)==2)
            {
                semantic_check(c2(node));
                if (FUN(c1(node)->type)->par_count >= 0)
                {
	                if (FUN(c1(node)->type)->par_count != nc(c2(node)))
    	                printerr("function call par mismatch");
             	   	int i;
             	   	for (i = 0; i < nc(c2(node)); ++i)
              	      	if (!same_type_cast(FUN(c1(node)->type)->par[i], c2(node)->child.list[i]->type))
               	         	printerr("function call par mismatch");
              	}
            }
            else
            	if (FUN(c1(node)->type)->par_count)
            		printerr("function call par mismatch");
            node -> type = FUN(c1(node)->type)->ret;
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case postfix_expression3:
        {
            semantic_check(c1(node));
            if (TN(c1(node)) == STRUCT || TN(c1(node)) == UNION)
            {
                Type* t = find_scope(ST(c1(node)->type), c2(node)->data);
                if (t)
                {
                    node -> type = t;
                    node -> isl = 1;
                    translate_expr(node);
                    break;
                }
            } 
            printerr("In a.b, b must be a field of a's type");
            break;
        }
        case postfix_expression4:
        {
            semantic_check(c1(node));
            if (TN(c1(node)) == POINTER)
            {
                Type* t = find_scope(ST(PTR(c1(node)->type)->type), c2(node)->data);
                if (t)
                {
                    node -> type = t;
                    node -> isl = 1;
                    translate_expr(node);
                    break;
                }
            } 
            printerr("a->b error");
            break; 
        }
        case postfix_expression5: 
        case postfix_expression6:
        {
            semantic_check(c1(node));
            if (is_scalar(TN(c1(node))))
                node -> type = c1(node)->type;
            else
                printerr("++/-- error");
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case argument_expression_list:
        {
            int i;
            IR *aug = 0;
		    for (i = 0; i < nc(node); ++i)
		    {
        		semantic_check(node -> child.list[i]);
        		if (TN(node->child.list[i]) == ARRAY)
        		{
        			IR_RECORD rec1 = reg(node->child.list[i]);
        			IR_RECORD rec2 = IR_new_address(4);
        			node -> ir = IR_merge(2, node -> ir, node -> child.list[i] -> ir);
        			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADS, 2, rec2, rec1));
        			aug = IR_merge(2, aug, IR_makelist(IR_AUG, 1, rec2));
        		}
        		else if (TN(node->child.list[i]) == STRUCT || TN(node->child.list[i]) == UNION)
        		{
        			IR_RECORD rec1 = IR_new_address(4);
        			IR_RECORD rec2 = IR_new_address(4);
        			IR_RECORD rec3 = IR_new_address(node->child.list[i]->type->size);
        			IR_RECORD rc = reg(node -> child.list[i]);
        			node -> ir = IR_merge(2, node -> ir, node -> child.list[i] -> ir);
        			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADS, 2, rec1, rec3));
        			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_ADS, 2, rec2, rc));        			
        			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_AUG, 1, rec1));
        			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_AUG, 1, rec2));
        			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_AUG, 1, IR_new_const(node->child.list[i]->type->size)));
        			node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_CALL, 1, IRmemcpy));
        			aug = IR_merge(2, aug, IR_makelist(IR_AUG, 1, rec1));
        		}
        		else
        		{
        			aug = IR_merge(2, aug, IR_makelist(IR_AUG, 1, reg(node -> child.list[i])));
        			node -> ir = IR_merge(2, node -> ir, node -> child.list[i]->ir);
        		}
        	}
        	node -> ir = IR_merge(2, node -> ir, aug);
            node -> isl = 0;
            break;
        }
        case unary_expression1:
        case unary_expression2:
        {
            semantic_check(c1(node));
            int cty = TN(c1(node));
            if (cty != INT && cty != CHAR && cty != POINTER)
                printerr("Type error");
            node -> type = c1(node)->type;
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case unary_expression3:
        case unary_expression4:
        {
            //semantic_check(c1(node));
        	if (calc_constexpr(node)) break;
            node -> type = &int_type;
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case unary_expression5:
        {
            semantic_check(c1(node));
            if (!c1(node)->isl)
            	printerr("Addressing rvalue");
            node -> type = &int_type;
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case unary_expression6:
        {
            semantic_check(c1(node));
            if (TN(c1(node)) == POINTER)
               	node -> type = PTR(c1(node)->type)->type;
            else if (TN(c1(node)) == ARRAY)
            	node -> type = ARR(c1(node)->type)->type;
            else
                printerr("deref error");
			node -> isl = 1;
			translate_expr(node);
            break;          
        }
        case unary_expression7:
        case unary_expression8:
        {
	        if (calc_constexpr(node)) break;        
            semantic_check(c1(node));
            if (TN(c1(node)) != INT && TN(c1(node)) != CHAR)
                printerr("unary +/- error");
            node -> type = c1(node)->type;
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case unary_expression9:
        {
	        if (calc_constexpr(node)) break;        
            semantic_check(c1(node));
            if (TN(c1(node)) != INT)
                printerr("unary ~ error");
            node -> type = &int_type;
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case unary_expression10:
        {
        	if (calc_constexpr(node)) break;            
            semantic_check(c1(node));        
            if (TN(c1(node)) == VOID || TN(c1(node)) == STRUCT || TN(c1(node)) == UNION)
                printerr("unary ! error");
            node -> type = &int_type;
            node -> isl = 0;
            translate_expr(node);
            break;  
        }
        case type_name:
        {
            semantic_check(c1(node));
            semantic_check(c2(node));
            node -> type = wrap_type(c1(node)->type, c2(node)->type);
            node -> isl = 0;
            break;
        }
        case cast_expression:
        {     
            semantic_check(c1(node));
            semantic_check(c2(node));           
            if ((TN(c1(node)) == STRUCT || TN(c1(node)) == UNION) && (TN(c2(node)) == STRUCT || TN(c2(node)) == UNION) && !same_type(c1(node)->type, c2(node)->type))
            	printerr("Two different types of struct cannot be cast into mutually");
            node -> type = c1(node)->type;
            node -> isl = 1;
            translate_expr(node);
            break;
        }
        case multiplicative_expression1:
        case multiplicative_expression2:
        case multiplicative_expression3:
        case and_expression:
        case exclusive_or_expression:
        case inclusive_or_expression:
        case shift_expression1:
        case shift_expression2:
        {
        	if (calc_constexpr(node)) break;           
            semantic_check(c1(node));
            semantic_check(c2(node));         
            int t1 = TN(c1(node));
            int t2 = TN(c2(node));
            if (t1 == CHAR && t2 == CHAR)
                node -> type = &char_type;
            else if ((t1 == CHAR || t1 == INT) && (t2 == CHAR || t2 == INT))
                node -> type = &int_type;
            else
                printerr("*/%% error");
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case additive_expression1:
        {
        	if (calc_constexpr(node)) break;         
            semantic_check(c1(node));
            semantic_check(c2(node));           
            int t1 = TN(c1(node));
            int t2 = TN(c2(node));
            if (t1 == CHAR && t2 == CHAR)
                node -> type = &char_type;
            else if ((t1 == CHAR || t1 == INT) && (t2 == CHAR || t2 == INT))
                node -> type = &int_type;
            else if ((t1 == POINTER || t1 == ARRAY) && (t2 == INT || t2 == CHAR))
                node -> type = c1(node)->type;
            else if ((t2 == POINTER || t2 == ARRAY) && (t1 == INT || t1 == CHAR))
                node -> type = c2(node)->type;
            else
                printerr("+ error");
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case additive_expression2:
        {
        	if (calc_constexpr(node)) break;           
            semantic_check(c1(node));
            semantic_check(c2(node));         
            int t1 = TN(c1(node));
            int t2 = TN(c2(node));
            if (t1 == CHAR && t2 == CHAR)
                node -> type = &char_type;
            else if (is_scalar(t1) && is_scalar(t2))
                node -> type = &int_type;
            else
                printerr("- error");
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case relational_expression1:
        case relational_expression2:
        case relational_expression3:
        case relational_expression4:
        case equality_expression1:
        case equality_expression2:
        {
        	if (calc_constexpr(node)) break;             
            semantic_check(c1(node));
            semantic_check(c2(node));       
            int t1 = TN(c1(node));
            int t2 = TN(c2(node));
            if (is_scalar(t1) && is_scalar(t2))
                node -> type = &int_type;
            else
                printerr("compare error");
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case logical_and_expression:
        {
        	if (calc_constexpr(node)) break;            
        	int fflag = 0;
        	if (logflag==0)
        	{
        		logflag=1;
        		fflag=1;
        		logical_false = IR_new_label();
        	}
            semantic_check(c1(node));
            semantic_check(c2(node));        
            int t1 = TN(c1(node));
            int t2 = TN(c2(node));
            if (is_scalar(t1) && is_scalar(t2))
                node -> type = &int_type;
            else
                printerr("logical expr error");
            node -> isl = 0;
            translate_expr(node);
            if (fflag)
            {
            	fflag=0;
            	logflag=0;
            	IR_RECORD lend = IR_new_label();
				IR_RECORD val = IR_new_address(4);  
            	node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_MOVE, 2, val, reg(node)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_GOTO, 1, lend));            	
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, logical_false));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_MOVE, 2, val, IR_new_const(0)));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_LAB, 1, lend));
				node -> ir = IR_merge(2, node -> ir, IR_makelist(IR_OR, 3, val, val, val));	
            }
            break;
        }        
        case logical_or_expression:
        {
        	if (calc_constexpr(node)) break;            
            semantic_check(c1(node));
            semantic_check(c2(node));        
            int t1 = TN(c1(node));
            int t2 = TN(c2(node));
            if (is_scalar(t1) && is_scalar(t2))
                node -> type = &int_type;
            else
                printerr("logical expr error");
            node -> isl = 0;
            translate_expr(node);
            break;
        }
        case assignment_expression1:
        {
        	if (calc_constexpr(node)) break;           
            semantic_check(c1(node));
            semantic_check(c2(node));         
            if (!c1(node)->isl)
            	printerr("rvalue assign");
            if (same_type_cast(c1(node)->type, c2(node)->type))
                node -> type = c1(node)->type;
            else
                printerr("= expr error");        
            node -> isl = 0;	
            translate_expr(node);
            break;
        }
        case assignment_expression2:
        case assignment_expression3:
        case assignment_expression4:
        case assignment_expression5:
        case assignment_expression6:
        case assignment_expression7:
        case assignment_expression8:
        case assignment_expression9:
        case assignment_expression10:
        case assignment_expression11:
        {
        	if (calc_constexpr(node)) break;        
            semantic_check(c1(node));
            semantic_check(c2(node));          
            if (!c1(node)->isl)
            	printerr("rvalue assign");
            int t1 = TN(c1(node));
            int t2 = TN(c2(node));
            if (is_scalar(t1) && is_scalar(t2))
                node -> type = c1(node)->type;
            else
                printerr("assignment expr error");     
            node -> isl = 0;   	
            translate_expr(node);
            break;
        }
        case expression:
        {
            int i;
		    for (i = 0; i < nc(node); ++i)
		    {
		    	if (!calc_constexpr(node -> child.list[i]))
	        		semantic_check(node -> child.list[i]);
        		node -> ir = IR_merge(2, node -> ir, node -> child.list[i]->ir);
        	}    	
            node -> type = cl(node)->type;
            node -> isl = 0;
            break;
        }
        case void_node:
        {
            node -> type = &void_type;
            node -> isl = 0;
            break;
        }
        default:
            printerr("WTF?!");
    }
}

