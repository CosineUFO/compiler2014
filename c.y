%{
#include "util.h"
#include "node.h"
#include "prtnode.h"
#include "env.h"
#include "semantic.h"

AST_Node *tRoot;
void yyerror(char *s);
FILE* tokens;
FILE* ast;
int phase;
int loop;
int scope;
int regc;
Type *nowret;
Type int_type;
Type char_type;
Type void_type;
%}

%union {
int iValue;
char *str;
AST_Node *nPtr;
};

%token BREAK_t CONTINUE_t ELSE_t FOR_t CHAR_t IF_t INT_t RETURN_t SIZEOF_t STRUCT_t UNION_t VOID_t WHILE_t
%token RIGHT_ASSIGN LEFT_ASSIGN ADD_ASSIGN SUB_ASSIGN MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN AND_ASSIGN XOR_ASSIGN OR_ASSIGN RIGHT_OP LEFT_OP INC_OP DEC_OP PTR_OP AND_OP OR_OP LE_OP GE_OP EQ_OP NE_OP
%token<str> STRING_LITERAL IDENTIFIER
%token<iValue> CHAR_CONSTANT INT_CONSTANT 

%type<nPtr> program declaration function_definition parameters declarator_list init_declarator_list init_declarator initializer initializer_list type_specifier type_declaration_list
%type<nPtr> type_declaration struct_or_union plain_declaration declarator declarator_array plain_declarator pointer statement compound_statement block_item_list block_item
%type<nPtr> expression_statement selection_statement iteration_statement jump_statement primary_expression string constant postfix_expression argument_expression_list expression
%type<nPtr> unary_expression type_name cast_expression multiplicative_expression additive_expression shift_expression relational_expression equality_expression
%type<nPtr> and_expression exclusive_or_expression inclusive_or_expression logical_and_expression logical_or_expression constant_expression assignment_expression identifier
%type<iValue> unary_operator assignment_operator

%left ','
%right '=' LEFT_ASSIGN RIGHT_ASSIGN MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN SUB_ASSIGN AND_ASSIGN XOR_ASSIGN OR_ASSIGN
%left OR_OP
%left AND_OP
%left '|'
%left '^'
%left EQ_OP NE_OP
%left '<' '>' LE_OP GE_OP
%left '*' '/' '%'
%right SIZEOF_t_OP '!' '~'

%start program
%%

program
: declaration                               {tRoot = $$ = make_node(program, 1, $1);}
| function_definition                       {tRoot = $$ = make_node(program, 1, $1);}
| program declaration                       {tRoot = $$ = append_ch($1, $2);}
| program function_definition               {tRoot = $$ = append_ch($1, $2);}
;

declaration
: type_specifier init_declarator_list ';'   {$$ = make_node(declaration1, 2, $1, $2);}
| type_specifier ';'                        {$$ = make_node(declaration2, 1, $1);}
;

function_definition
: type_specifier plain_declarator '(' parameters ')' compound_statement     {$$ = make_node(function_definition1, 4, $1, $2, $4, $6);}
| type_specifier plain_declarator '(' ')' compound_statement                {$$ = make_node(function_definition2, 3, $1, $2, $5);}
;

parameters
: plain_declaration                         {$$ = make_node(parameters, 1, $1);}
| parameters ',' plain_declaration          {$$ = append_ch($1, $3);}
;

declarator_list
: declarator                                {$$ = make_node(declarator_list, 1, $1);}
| declarator_list ',' declarator            {$$ = append_ch($1, $3);}
;

init_declarator_list
: init_declarator                           {$$ = make_node(init_declarator_list, 1, $1);}
| init_declarator_list ',' init_declarator  {$$ = append_ch($1, $3);}
;

init_declarator
: declarator '=' initializer                {$$ = make_node(init_declarator, 2, $1, $3);}
| declarator                                {$$ = $1;}
;

initializer
: assignment_expression                     {$$ = $1;}
| '{' initializer_list '}'                  {$$ = $2;}
;

initializer_list
: initializer                               {$$ = make_node(initializer_list, 1, $1);}
| initializer_list ',' initializer          {$$ = append_ch($1, $3);}
;

type_specifier
: VOID_t                                                          {$$ = make_node(type_specifier1, 0);}
| CHAR_t                                                          {$$ = make_node(type_specifier2, 0);}
| INT_t                                                           {$$ = make_node(type_specifier3, 0);}
| struct_or_union IDENTIFIER '{' type_declaration_list '}'	  	  {$$ = make_node(type_specifier4, 3, $1, make_id($2), $4);}
| struct_or_union '{' type_declaration_list '}'			 		  {$$ = make_node(type_specifier5, 2, $1, $3);}
| struct_or_union IDENTIFIER					 				  {$$ = make_node(type_specifier6, 2, $1, make_id($2));}
;

type_declaration_list
: type_declaration ';'									{$$ = make_node(type_declaration_list, 1, $1);}
| type_declaration_list type_declaration ';'			{$$ = append_ch($1, $2);}
;

type_declaration
: type_specifier declarator_list				{$$ = make_node(type_declaration, 2, $1, $2);}
;

struct_or_union
: STRUCT_t			{$$ = make_node(struct_or_union1, 0);}
| UNION_t			{$$ = make_node(struct_or_union2, 0);}
;

plain_declaration
: type_specifier declarator		{$$ = make_node(plain_declaration, 2, $1, $2);}
;

declarator
: plain_declarator '(' parameters ')'			{$$ = make_node(declarator1, 2, $1, $3);}
| plain_declarator '(' ')'						{$$ = make_node(declarator2, 1, $1);}
| declarator_array								{$$ = $1;}
;

declarator_array
: plain_declarator									{$$ = $1;}
| declarator_array '[' constant_expression ']'		{$$ = make_node(declarator_array, 2, $1, $3);}
;

plain_declarator
: IDENTIFIER			{$$ = make_id($1);}
| pointer IDENTIFIER	{$$ = make_node(plain_declarator, 2, $1, make_id($2));}
;

pointer
: '*'					{$$ = make_node(pointer, 1, make_node(void_node, 0));}
| pointer '*'			{$$ = make_node(pointer, 1, $1);}
;

statement
: compound_statement	{$$ = make_node(statement, 1, $1);}
| expression_statement	{$$ = make_node(statement, 1, $1);}
| selection_statement	{$$ = make_node(statement, 1, $1);}
| iteration_statement	{$$ = make_node(statement, 1, $1);}
| jump_statement		{$$ = make_node(statement, 1, $1);}
;

compound_statement
: '{' '}'					{$$ = make_node(compound_statement, 0);}
| '{'  block_item_list '}'  {$$ = make_node(compound_statement, 1, $2);}
;

block_item_list
: block_item						{$$ = make_node(block_item_list, 1, $1);}
| block_item_list block_item		{$$ = append_ch($1, $2);}
;

block_item
: declaration				{$$ = $1;}
| statement					{$$ = $1;}
;

expression_statement
: ';'						{$$ = make_node(expression_statement, 0);}
| expression ';'			{$$ = $1;}
;

selection_statement
: IF_t '(' expression ')' statement ELSE_t statement			{$$ = make_node(selection_statement1, 3, $3, $5, $7);}
| IF_t '(' expression ')' statement								{$$ = make_node(selection_statement2, 2, $3, $5);}
;

iteration_statement
: WHILE_t '(' expression ')' statement								{$$ = make_node(iteration_statement1, 2, $3, $5);}
| FOR_t '(' expression_statement expression_statement ')' statement				{$$ = make_node(iteration_statement2, 3, $3, $4, $6);}
| FOR_t '(' expression_statement expression_statement expression ')' statement			{$$ = make_node(iteration_statement3, 4, $3, $4, $5, $7);}
;

jump_statement
: CONTINUE_t ';'				{$$ = make_node(jump_statement1, 0);}
| BREAK_t ';'					{$$ = make_node(jump_statement2, 0);}
| RETURN_t ';'					{$$ = make_node(jump_statement3, 0);}
| RETURN_t expression ';'		{$$ = make_node(jump_statement4, 1, $2);}
;

primary_expression
: identifier					{$$ = make_node(primary_expression1, 1, $1);}
| constant						{$$ = make_node(primary_expression2, 1, $1);}
| string						{$$ = make_node(primary_expression3, 1, $1);}
| '(' expression ')'			{$$ = make_node(primary_expression4, 1, $2);}
;

identifier
: IDENTIFIER					{$$ = make_id($1);}

string
: STRING_LITERAL				{$$ = make_str($1);}
;

constant
: INT_CONSTANT					{$$ = make_int($1);}
| CHAR_CONSTANT					{$$ = make_char($1);}
;

postfix_expression
: primary_expression										{$$ = $1;}
| postfix_expression '[' expression ']'						{$$ = make_node(postfix_expression1, 2, $1, $3);}
| postfix_expression '(' ')'								{$$ = make_node(postfix_expression2, 1, $1);}
| postfix_expression '(' argument_expression_list ')'		{$$ = make_node(postfix_expression2, 2, $1, $3);}
| postfix_expression '.' IDENTIFIER							{$$ = make_node(postfix_expression3, 2, $1, make_id($3));}
| postfix_expression PTR_OP IDENTIFIER						{$$ = make_node(postfix_expression4, 2, $1, make_id($3));}
| postfix_expression INC_OP									{$$ = make_node(postfix_expression5, 1, $1);}
| postfix_expression DEC_OP									{$$ = make_node(postfix_expression6, 1, $1);}
;

argument_expression_list
: assignment_expression										{$$ = make_node(argument_expression_list, 1, $1);}
| argument_expression_list ',' assignment_expression		{$$ = append_ch($1, $3);}
;

unary_expression										
: postfix_expression										{$$ = $1;}
| INC_OP unary_expression									{$$ = make_node(unary_expression1, 1, $2);}
| DEC_OP unary_expression									{$$ = make_node(unary_expression2, 1, $2);}
| unary_operator cast_expression							{$$ = make_node($1, 1, $2);}
| SIZEOF_t unary_expression									{$$ = make_node(unary_expression3, 1, $2);}
| SIZEOF_t '(' type_name ')'								{$$ = make_node(unary_expression4, 1, $3);}
;

type_name
: type_specifier										{$$ = $1;}
| type_specifier pointer								{$$ = make_node(type_name, 2, $1, $2);}
;


unary_operator
: '&'													{$$ = unary_expression5;}
| '*'													{$$ = unary_expression6;}
| '+'													{$$ = unary_expression7;}
| '-'													{$$ = unary_expression8;}
| '~'													{$$ = unary_expression9;}
| '!'													{$$ = unary_expression10;}
;

cast_expression
: unary_expression								{$$ = $1;}
| '(' type_name ')' cast_expression				{$$ = make_node(cast_expression, 2, $2, $4);}
;
multiplicative_expression
: cast_expression										{$$ = $1;}
| multiplicative_expression '*' cast_expression			{$$ = make_node(multiplicative_expression1, 2, $1, $3);}
| multiplicative_expression '/' cast_expression			{$$ = make_node(multiplicative_expression2, 2, $1, $3);}
| multiplicative_expression '%' cast_expression			{$$ = make_node(multiplicative_expression3, 2, $1, $3);}
;

additive_expression
: multiplicative_expression								{$$ = $1;}
| additive_expression '+' multiplicative_expression		{$$ = make_node(additive_expression1, 2, $1, $3);}
| additive_expression '-' multiplicative_expression		{$$ = make_node(additive_expression2, 2, $1, $3);}
;

shift_expression
: additive_expression									{$$ = $1;}
| shift_expression LEFT_OP additive_expression			{$$ = make_node(shift_expression1, 2, $1, $3);}
| shift_expression RIGHT_OP additive_expression			{$$ = make_node(shift_expression2, 2, $1, $3);}
;

relational_expression
: shift_expression										{$$ = $1;}
| relational_expression '<' shift_expression			{$$ = make_node(relational_expression1, 2, $1, $3);}
| relational_expression '>' shift_expression			{$$ = make_node(relational_expression2, 2, $1, $3);}
| relational_expression LE_OP shift_expression			{$$ = make_node(relational_expression3, 2, $1, $3);}
| relational_expression GE_OP shift_expression			{$$ = make_node(relational_expression4, 2, $1, $3);}
;

equality_expression
: relational_expression										{$$ = $1;}
| equality_expression EQ_OP relational_expression			{$$ = make_node(equality_expression1, 2, $1, $3);}
| equality_expression NE_OP relational_expression			{$$ = make_node(equality_expression2, 2, $1, $3);}
;

and_expression
: equality_expression										{$$ = $1;}
| and_expression '&' equality_expression					{$$ = make_node(and_expression, 2, $1, $3);}
;

exclusive_or_expression
: and_expression											{$$ = $1;}
| exclusive_or_expression '^' and_expression				{$$ = make_node(exclusive_or_expression, 2, $1, $3);}
;

inclusive_or_expression
: exclusive_or_expression									{$$ = $1;}
| inclusive_or_expression '|' exclusive_or_expression		{$$ = make_node(inclusive_or_expression, 2, $1, $3);}
;

logical_and_expression
: inclusive_or_expression									{$$ = $1;}
| logical_and_expression AND_OP inclusive_or_expression		{$$ = make_node(logical_and_expression, 2, $1, $3);}
;

logical_or_expression
: logical_and_expression									{$$ = $1;}
| logical_or_expression OR_OP logical_and_expression		{$$ = make_node(logical_or_expression, 2, $1, $3);}
;

constant_expression
: logical_or_expression										{$$ = $1;}
;

assignment_expression
: logical_or_expression											{$$ = $1;}
| unary_expression assignment_operator assignment_expression	{$$ = make_node($2, 2, $1, $3);}
;

assignment_operator
: '='					{$$ = assignment_expression1;}
| MUL_ASSIGN			{$$ = assignment_expression2;}
| DIV_ASSIGN			{$$ = assignment_expression3;}
| MOD_ASSIGN			{$$ = assignment_expression4;}
| ADD_ASSIGN			{$$ = assignment_expression5;}
| SUB_ASSIGN			{$$ = assignment_expression6;}
| LEFT_ASSIGN			{$$ = assignment_expression7;}
| RIGHT_ASSIGN			{$$ = assignment_expression8;}
| AND_ASSIGN			{$$ = assignment_expression9;}
| XOR_ASSIGN			{$$ = assignment_expression10;}
| OR_ASSIGN				{$$ = assignment_expression11;}
;

expression	
: assignment_expression						{$$ = $1;}
| expression ',' assignment_expression		{$$ = make_node(expression, 2, $1, $3);}
;

%%

extern char yytext[];

void yyerror(char *s)
{
	printerr(s);
}

extern FILE *yyin;

void init_types()
{
	int_type.type_class = INT;
	int_type.size = 4;
	char_type.type_class = CHAR;
	char_type.size = 1;
	void_type.type_class = VOID;
	void_type.size = 4;
}

void init_printf()
{
	Type *pf = checked_malloc(sizeof(Type));
	pf -> type_class = FUNCTION;
	pf -> type_data = checked_malloc(sizeof(Function));
	FUN(pf)->par_count = -1;
	FUN(pf)->ret = &void_type;
	add_var("printf", pf);
}

void init_memcpy()
{
	Type *sf = checked_malloc(sizeof(Type));
	sf -> type_class = FUNCTION;
	sf -> type_data = checked_malloc(sizeof(Function));
	FUN(sf)->par_count = 2;
	FUN(sf)->par = checked_malloc(sizeof(Type*)*3);
	FUN(sf)->par[0] = pointer_to(&void_type);
	FUN(sf)->par[1] = pointer_to(&void_type);
	FUN(sf)->par[2] = &int_type;
	FUN(sf)->ret = &void_type;
	add_var("memcpy", sf);
}

void init_malloc()
{
	Type *ma = checked_malloc(sizeof(Type));
	ma -> type_class = FUNCTION;
	ma -> type_data = checked_malloc(sizeof(Function));
	FUN(ma)->par_count = 1;
	FUN(ma)->par = checked_malloc(sizeof(Type*));
	FUN(ma)->par[0] = &int_type;
	FUN(ma)->ret = pointer_to(&void_type);
	add_var("malloc", ma);
}

void init_global_vars()
{
	nowret = NULL;
	regc = 0;
	tRoot = NULL;
	loop = 0;
	scope = 1;
	clean_scope(scope);
}

int main(int argc, char *argv[])
{
	phase = 0;
	init_global_vars();
	init_types();
	tokens = fopen("Data.tokens", "w");
	ast = fopen("Data.ast", "w");
    yyin = fopen(argv[1], "r");
    yyparse();
	//print_node(tRoot, 0);
	fclose(tokens);
	fclose(ast);
	phase = 1;
	fflush(stdout);
	init_printf();
	init_memcpy();
	init_malloc();
	semantic_check(tRoot);
	prtIR(tRoot->ir);
	free_node(tRoot);
    fclose(yyin);
    return 0;
}

