#include "env.h"

static struct ENV_DEF env;
IR *mainIR = NULL;
IR *globalIR = NULL;
IR_RECORD IRmemcpy, IRmalloc;

void clean_scope(int scope)
{
	int i;
	for (i = 0; i < PRIME; ++i)
	{
		env.ventry[scope][i] = NULL;
		env.tentry[scope][i] = NULL;
	}
}

void begin_scope()
{
	clean_scope(scope);
    ++scope;
}

void end_scope()
{
    --scope;
}

void hold_begin_scope()
{
    ++scope;
}

void hold_end_scope()
{
    --scope;
}

IR_RECORD add_var(char *id, Type *type)
{
	IR_RECORD rec;
	if (type->type_class == FUNCTION)
	{
		rec = IR_new_label();
		if (strcmp(id, "main") == 0)
			mainIR = IR_makelist(IR_CALL, 1, rec);
		else if (strcmp(id, "memcpy") == 0)
			IRmemcpy = rec;
		else if (strcmp(id, "malloc") == 0)
			IRmalloc = rec;
	}
	else
	{
		rec = IR_new_address(type->size);
	}
	h_enter(id, type, rec, env.ventry[scope-1]);
	return rec;
}

IR_RECORD add_var_sym(char *id, Type *type)
{
	IR_RECORD rec;
	rec = IR_new_symbol(type->size);
	h_enter(id, type, rec, env.ventry[scope-1]);
	return rec;
}

void add_type(char *id, Type *type)
{
	IR_RECORD rec;
	h_enter(id, type, rec, env.tentry[scope-1]);
}

void def_type(char *id, Type *type)
{
	Type *p = h_look(id, env.tentry[scope-1]);
	p -> type_class = type -> type_class;
	p -> type_data = type -> type_data;
	p -> size = type -> size;
}

Type *look_var(char *id)
{
	int i = scope - 1;
	for (; i >= 0; --i)
	{
		Type *p = h_look(id, env.ventry[i]);
		if (p) return p;
	}
	return NULL;
}

Type *look_type(char *id)
{
	int i = scope - 1;
	for (; i >= 0; --i)
	{
		Type *p = h_look(id, env.tentry[i]);
		if (p) return p;
	}
	return NULL;
}

Type *look_var_top(char *id)
{
	return h_look(id, env.ventry[scope-1]);
}

Type *look_type_top(char *id)
{
	return h_look(id, env.tentry[scope-1]);
}

