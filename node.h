#ifndef NODE_H
#define NODE_H

#include "util.h"
#include "types.h"
#include "IR.h"

#define c1(n) ((n)->child.list[0])
#define c2(n) ((n)->child.list[1])
#define c3(n) ((n)->child.list[2])
#define c4(n) ((n)->child.list[3])
#define nc(n) ((n)->child.count)
#define cl(n) ((n)->child.list[nc(n)-1])
#define TN(n) ((n)->type->type_class)
#define CValue(n) (TN(n)==INT ? *(int*)((n) -> data) : *(char*)((n) -> data))

extern Type int_type;
extern Type char_type;
extern Type void_type;

typedef enum//22
{
    program,
    declaration1,
    declaration2,
    function_definition1,
    function_definition2,
    parameters,
    declarator_list,
    init_declarator_list,
    init_declarator,
    initializer_list,
    type_specifier1,
    type_specifier2,
    type_specifier3,
    type_specifier4,
    type_specifier5,
    type_specifier6,
    type_declaration_list,
    type_declaration,
    struct_or_union1,
    struct_or_union2,
    plain_declaration,
    declarator1,
    declarator2,
    declarator_array,
    plain_declarator,
    pointer,
    statement, 
    compound_statement,
    block_item_list,
    expression_statement,
    selection_statement1,
    selection_statement2,
    iteration_statement1,
    iteration_statement2,
    iteration_statement3,
    jump_statement1,
    jump_statement2,
    jump_statement3,
    jump_statement4,
    primary_expression1,
    primary_expression2,
    primary_expression3,
    primary_expression4,
    postfix_expression1,
    postfix_expression2,
    postfix_expression3,
    postfix_expression4,
    postfix_expression5,
    postfix_expression6,
    argument_expression_list,
    unary_expression1,								
    unary_expression2,	
    unary_expression3,								
    unary_expression4,	
    unary_expression5,								
    unary_expression6,	
    unary_expression7,								
    unary_expression8,	
    unary_expression9,								
    unary_expression10,	
    type_name,
    cast_expression,
    multiplicative_expression1,
    multiplicative_expression2,
    multiplicative_expression3,
    additive_expression1,
    additive_expression2,
    shift_expression1,
    shift_expression2,
    relational_expression1,
    relational_expression2,
    relational_expression3,
    relational_expression4,
    equality_expression1,
    equality_expression2,
    and_expression,
    exclusive_or_expression,
    inclusive_or_expression,
    logical_and_expression,
    logical_or_expression,
    assignment_expression1,
    assignment_expression2,
    assignment_expression3,
    assignment_expression4,
    assignment_expression5,
    assignment_expression6,
    assignment_expression7,
    assignment_expression8,
    assignment_expression9,
    assignment_expression10,
    assignment_expression11,
    expression,
    literal_node,
    identifier_node,
    void_node
} node_type;

typedef struct AST_Child_def
{
	int count;
    struct AST_Node_def **list;
} AST_Child;

typedef struct AST_Node_def
{
    node_type ntype;
    int isl;
    Type *type;
    IR *ir;
    union
    {
    	AST_Child child;
    	void *data;
    };
} AST_Node;

AST_Node *make_node(node_type, int, ...);
AST_Node *make_int(int n);
AST_Node *make_char(char c);
AST_Node *make_str(char *str);
AST_Node *make_id(char *str);
void free_node(AST_Node *);
AST_Node *append_ch(AST_Node *, AST_Node *);

#endif
