#include "util.h"
void printerr(const char* str)
{
	if (phase == 0)
		fprintf(stderr, "Row %d Column %d : ", row + 1, column + 1);
	fprintf(stderr, "%s\n", str);
	exit(1);
}
void* checked_malloc(size_t len)
{
	void *p = malloc(len);
	if (!p)
		printerr("Ran out of memory!");
#ifdef DEBUG
	printf("%d %d\n", p, len);
#endif
	return p;
}
