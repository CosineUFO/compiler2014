#ifndef SEMANTIC_H
#define SEMANTIC_H

#include "util.h"
#include "env.h"
#include "types.h"
#include "node.h"

#define reg(n) ((n)->ir->instr[(n)->ir->num-1].rec[0])

void semantic_check(AST_Node *);

void semantic_check_plain_declaration(AST_Node *);
void semantic_check_declarator(AST_Node *, Type *, int);
void semantic_check_init_declarator(AST_Node *, Type *);
void semantic_check_declarator_array(AST_Node *, Type *, int);
void semantic_check_plain_declarator(AST_Node *, Type *);
void semantic_check_identifier_node(AST_Node *, Type *);
void semantic_check_declaration(AST_Node *);
int calc_constexpr(AST_Node *);
void translate_expr(AST_Node *);
char *get_declarator_id(AST_Node *);

extern Type *nowret;

#endif
