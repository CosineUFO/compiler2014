OBJDIR = ./obj
OUTDIR = ./bin
CC     = gcc
CFLAG  = -Wall -O2 -o
OBJECTS= $(OBJDIR)/env.o  $(OBJDIR)/node.o  $(OBJDIR)/prtnode.o $(OBJDIR)/semantic.o $(OBJDIR)/table.o $(OBJDIR)/types.o $(OBJDIR)/util.o $(OBJDIR)/IR.o

all : $(OBJDIR) $(OUTDIR) bison flex translater interpreter main

$(OBJDIR) :
	mkdir $@

$(OUTDIR) :
	mkdir $@
        
bison : c.y
	bison -d c.y
		
flex : c.l
	flex c.l

$(OBJDIR)/%.o : %.c
	$(CC) -c $(CFLAG) $@ $<

translater : $(OBJECTS)
	$(CC) $(CFLAG) $(OUTDIR)/translater $(OBJECTS) lex.yy.c c.tab.c

interpreter : interpreter.c
	$(CC) $(CFLAG) $(OUTDIR)/interpreter interpreter.c
	
main : main.c
	$(CC) $(CFLAG) $(OUTDIR)/main main.c
        
clean : 
	rm -f c.tab.* lex.yy.c *.ast *.tokens *.s *.t
	rm -rf $(OBJDIR)
	rm -rf $(OUTDIR)
