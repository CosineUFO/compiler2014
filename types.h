#ifndef TYPES_H
#define TYPES_H

#include "util.h"

#define is_scalar(t) ((t) == CHAR || (t) == INT || (t) == POINTER) 

#define ARR(n)  ((Array*)(n)->type_data)
#define PTR(n)  ((Pointer*)(n)->type_data)
#define ST(n)   ((Struct*)(n)->type_data)
#define UN(n)   ((Union*)(n)->type_data)
#define FUN(n) ((Function*)(n)->type_data)

typedef enum
{
    INT,
    CHAR,
    VOID,
    ARRAY,
    STRUCT,
    UNION,
    FUNCTION,
    POINTER
} Type_enum; 			 //所有基本类型的枚举

typedef struct Type_def	 //所有“类型”如int, char, struct, func...的包装。只是类型，不包含具体对象信息！
{
    Type_enum type_class; //具体是什么类型？eg. int or char or union...
    void *type_data;	 //对于struct，union，func，array，里面存必要的信息，如struct存field，func存ret+par list
    size_t size;	  //类型大小
} Type;

typedef struct Array_def //数组，对应一个type_class为ARRAY的type中type_data部分，一下几个类似
{
    Type *type;          //基类型
    int capacity;	//数组大小
} Array;

typedef struct Struct_def //结构体
{
    int field_cnt;     //域的数量
    Type **field;      //域数组
    char **field_name; //域名数组
} Struct;

typedef Struct Union; //联合体

typedef struct Pointer_def //指针
{
    Type *type;         //基类型
} Pointer;

typedef struct Function_def //函数
{
    Type *ret;          //返回类型
    Type **par;		   //参数表
    int par_count;     //参数个数
    //考虑到printf和scanf，是否此处应增加一个域表示变长参数？
} Function;

Type *find_scope(Struct *, char *);//在一个struct里寻找指定名字的类
int calc_offset(Struct *, char *);
Type *new_array_type();
Type *new_pointer_type();
Type *new_struct_type();
Type *new_union_type();
Type *pointer_to(Type *);
Type *array_of(Type *, int);
Type *func_decl(Type *, Type **, int);
Type *wrap_type(Type *, Type *);
int same_type(Type *, Type *);
int same_type_cast(Type *, Type *);

#endif
