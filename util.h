#ifndef UTIL_H
#define UTIL_H
//#define DEBUG
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>

#define align(n) ((n) & ~3) + (((n) & 3) ? 4 : 0)

extern int row;
extern int column;
extern int loop;
extern int scope;
extern int phase;
extern int regc;

void printerr(const char*);
void* checked_malloc(size_t);

#endif
