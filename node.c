#include "node.h"

AST_Node *append_ch(AST_Node *p, AST_Node *c)
{
	int i = nc(p);
    AST_Node **l = checked_malloc(sizeof(AST_Node*) * (i+1));
    memcpy(l, p->child.list, sizeof(AST_Child*) * i);
    l[i] = c;
    free(p->child.list);
    p->child.list = l;
    ++nc(p);
    return p;
}

AST_Node *make_node(node_type t, int n, ...)
{
    AST_Node *p = checked_malloc(sizeof(AST_Node));
    p->ntype = t;
    p->type = NULL;
    p->isl = 0;
    nc(p) = n;
    if (n)
    {
   		p->child.list = checked_malloc(sizeof(AST_Child*) * n);
   		va_list args;
    	va_start(args, n);
    	int i;
    	for (i = 0; i < n; ++i)
    		p->child.list[i] = va_arg(args, AST_Node*);
 	   	va_end(args);
 	}
 	else
 		p->child.list = NULL;
    p->ir = NULL;
    return p;
}

void free_node(AST_Node *p)
{
    if (p)
    {
        if (p->ntype == literal_node || p->ntype == identifier_node)
        	free(p->data);
        else
        {
        	int i;
        	for (i = 0; i < nc(p); ++i)
        		free_node(p->child.list[i]);
        	free(p->child.list);
        }
        free(p);
    }
}

AST_Node *make_int(int n)
{
	AST_Node *p = checked_malloc(sizeof(AST_Node));
	p->ntype = literal_node;
	p->isl = 0;
	p->type = &int_type;
    p->data = checked_malloc(sizeof(int));
    *(int*)(p->data) = n;
    p->ir = NULL;
    return p;
}

AST_Node *make_char(char c)
{
	AST_Node *p = checked_malloc(sizeof(AST_Node));
	p->ntype = literal_node;
	p->isl = 0;
	p->type = &char_type;
    p->data = checked_malloc(sizeof(char));
    *(char*)(p->data) = c;
    p->ir = NULL;
    return p;
}

AST_Node *make_str(char* str)
{
	AST_Node *p = checked_malloc(sizeof(AST_Node));
	p->isl = 0;
	p->ntype = literal_node;
	p->type = pointer_to(&char_type);
    p->data = strdup(str);
    p->ir = NULL;
    return p;
}

AST_Node *make_id(char* str)
{
	AST_Node *p = checked_malloc(sizeof(AST_Node));
	p->isl = 1;
	p->ntype = identifier_node;
	p->type = NULL;
    p->data = strdup(str);
    p->ir = NULL;
    return p;
}

