#include "types.h"

Type *find_scope(Struct *s, char *str)
{
    int i;
    for (i = 0; i < s -> field_cnt; ++i)
    	if (strcmp(str, s->field_name[i])==0)
    	    return s->field[i];
    return NULL;
}

int calc_offset(Struct *s, char *str)
{
    int i, offset = 0;
    for (i = 0; i < s -> field_cnt; ++i)
    {
    	if (strcmp(str, s->field_name[i])==0)
    	    return offset;
    	offset += align(s->field[i]->size);
    }
    printerr("What The FXXK?!");
    return 0;
}

Type *new_array_type()
{
	Type *ret = checked_malloc(sizeof(Type));
	ret -> type_class = ARRAY;
	ret -> type_data = NULL;
	ret -> size = 4;
	return ret;
}

Type *new_pointer_type()
{
	Type *ret = checked_malloc(sizeof(Type));
	ret -> type_class = POINTER;
	ret -> type_data = NULL;
	ret -> size = 4;
	return ret;
}

Type *new_struct_type()
{
	Type *ret = checked_malloc(sizeof(Type));
	ret -> type_class = STRUCT;
	ret -> type_data = checked_malloc(sizeof(Struct));
	ST(ret) -> field_cnt = 0;
	ST(ret) -> field = NULL;	
	ST(ret) -> field_name = NULL;	
	ret -> size = 0;
	return ret;
}

Type *new_union_type()
{
	Type *ret = checked_malloc(sizeof(Type));
	ret -> type_class = UNION;
	ret -> type_data = checked_malloc(sizeof(Union));
	UN(ret) -> field_cnt = 0;
	UN(ret) -> field = NULL;
	UN(ret) -> field_name = NULL;
	ret -> size = 0;
	return ret;
}

Type *pointer_to(Type *type)
{
	Type *ret = checked_malloc(sizeof(Type));
	ret -> type_class = POINTER;
	ret -> type_data = checked_malloc(sizeof(Pointer));
	ret -> size = 4;
	PTR(ret) -> type = type;
	return ret;
}

Type *array_of(Type *type, int capacity)
{
	Type *ret = checked_malloc(sizeof(Type));
	ret -> type_class = ARRAY;
	ret -> type_data = checked_malloc(sizeof(Array));
	ret -> size = type -> size * capacity;
	ARR(ret) -> capacity = capacity;
	ARR(ret) -> type = type;
	return ret;
}

Type *func_decl(Type *returntype, Type **arg, int arg_count)
{
    Type *fun = checked_malloc(sizeof(Type));
    fun -> type_class = FUNCTION;
    fun -> type_data = checked_malloc(sizeof(Function));
    fun -> size = 0;
    FUN(fun) -> ret = returntype;
    FUN(fun) -> par_count = arg_count;
    FUN(fun) -> par = arg;
    return fun;
}

int same_type(Type *t1, Type *t2)
{
	if (t1 == NULL || t2 == NULL) return 0;
	if (t1 -> type_class != t2 -> type_class) return 0;
	switch (t1->type_class)
	{
		case INT:
		case CHAR:
		case VOID:
			return 1;
		case POINTER:
			return PTR(t1)->type->type_class == VOID || PTR(t2)->type->type_class == VOID || same_type(PTR(t1)->type, PTR(t2)->type);
		case ARRAY:
			if (ARR(t1)->capacity != ARR(t2)->capacity) return 0;
			return same_type(ARR(t1)->type, ARR(t2)->type);
		case FUNCTION:
			if (FUN(t1)->par_count != FUN(t2)->par_count) return 0;
			if (!same_type(FUN(t1)->ret, FUN(t2)->ret)) return 0;
			int i;
			for (i = 0; i < FUN(t1)->par_count; ++i)
				if (!same_type(FUN(t1)->par[i], FUN(t2)->par[i])) return 0;
			return 1;
		case STRUCT:
		case UNION:
			return (t1 == t2);
	}
	return 1;
}

int same_type_cast(Type *t1, Type *t2)
{
	if (t1 == NULL || t2 == NULL) return 0;
	if ((t1 -> type_class == INT || t1 -> type_class == CHAR || t1 -> type_class == POINTER) && (t2 -> type_class == INT || t2 -> type_class == CHAR || t2 -> type_class == POINTER) && (t1 -> type_class != POINTER || t2 -> type_class != POINTER)) return 1;
	if (t1 -> type_class == POINTER && t2 -> type_class == ARRAY && same_type_cast(ARR(t1) -> type, PTR(t2) -> type)) return 1;
	if (t1 -> type_class == ARRAY && t2 -> type_class == POINTER && same_type_cast(PTR(t1) -> type, ARR(t2) -> type)) return 1;
	return same_type(t1, t2);
}

Type *wrap_type(Type *p, Type *t)
{
    while(t->type_class == ARRAY || t->type_class == POINTER)
    {
        if(t->type_class == ARRAY)
        {
            p = array_of(p, ARR(t)->capacity);
            t = ARR(t)->type;
        }
        else
        {
            p = pointer_to(p);
            t = PTR(t)->type;
        }
    }
    return p;
}
