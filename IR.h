#ifndef IR_H
#define IR_H

#include "util.h"

typedef enum
{
    IR_LAB,
    IR_GOTO,
    IR_SHL,
    IR_SHR,
    IR_MUL,
    IR_DIV,
    IR_MOD,
    IR_ADD,
    IR_SUB,
    IR_AND,
    IR_XOR,
    IR_OR,
    IR_EQ,
    IR_NE,
    IR_LE,
    IR_GE,
    IR_GT,
    IR_LT,
    IR_NOT,
    IR_LNOT,
    IR_IF,
    IR_CALL,
    IR_RET,
    IR_PUSH,
    IR_POP,
    IR_TINT,
    IR_TCHAR,
    IR_MOVE,
    IR_ADS,
    IR_STAR,
    IR_FUNC,
    IR_END,
    IR_AUG,
    IR_GAUG
} IR_OP;

typedef struct
{
    enum
    {
        IR_CONST,
        IR_ADDRESS,
        IR_LABEL,
        IR_SYMBOL
    } type;
    int flag;
    union
    {
    	int record;
    	char *recordstr;
    };
} IR_RECORD;

typedef struct
{
    IR_OP op;
    IR_RECORD rec[3];
} IR_INSTR;

typedef struct IR
{
    int num;
    int size;
    IR_INSTR *instr;
} IR;

IR_RECORD IR_new_const(int);
IR_RECORD IR_new_string(char*);
IR_RECORD IR_new_address(int);
IR_RECORD IR_new_label();
IR_RECORD IR_new_symbol(int);
IR * IR_new();
void IR_enlarge(IR *);
IR * IR_makelist(IR_OP, int, ...);
IR * IR_merge(int, ...);
void prtIR(IR *);
void prtrec(IR_RECORD);
int neqr(IR_RECORD, IR_RECORD);

#endif
